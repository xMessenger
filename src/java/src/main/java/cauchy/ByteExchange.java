package cauchy;

import java.security.SecureRandom;

/**
 * <p>short, int, long, float, double 类型与 byte[] 类型的转换类，包含LSB和MSB实现。</p>
 *
 * @author <a href="mailto:cauchy.song@gmail.com">Cauchy Song</a>
 * @author <a href="mailto:dongsheng.song@gmail.com">Dongsheng Song</a>
 * @version 1.2
 */
public class ByteExchange {

    private ByteExchange() { }

    /**
     * 使用高度随机数值测试所有方法
     * @param args 忽略
     */
    public static void main(String[] args) {
        int i;
        SecureRandom rand = new SecureRandom();
        for(i = 0; i < 1000; i++) {
            short i2 = (short)rand.nextInt();
            int i4 = rand.nextInt();
            long i8 = rand.nextLong();
            float f4 = rand.nextFloat();
            double f8 = rand.nextDouble();
            byte[] bs;

            // test i2
            bs = toMSB(i2);
            if(i2 != MSB2Short(bs)) {
                System.err.println("toMSB or MSB2Short fail\n");
                System.exit(1);
            }
            bs = toLSB(i2);
            if(i2 != LSB2Short(bs)) {
                System.err.println("toLSB or LSB2Short fail\n");
                System.exit(1);
            }

            // test i4
            bs = toMSB(i4);
            if(i4 != MSB2Int(bs)) {
                System.err.println("toMSB or MSB2Int fail\n");
                System.exit(1);
            }
            bs = toLSB(i4);
            if(i4 != LSB2Int(bs)) {
                System.err.println("toLSB or LSB2Int fail\n");
                System.exit(1);
            }

            // test f4
            bs = toMSB(f4);
            if(f4 != MSB2Float(bs)) {
                System.err.println("toMSB or MSB2Float fail\n");
                System.exit(1);
            }
            bs = toLSB(f4);
            if(f4 != LSB2Float(bs)) {
                System.err.println("toLSB or LSB2Float fail\n");
                System.exit(1);
            }

            // test i8
            bs = toMSB(i8);
            if(i8 != MSB2Long(bs)) {
                System.err.println("toMSB or MSB2Long fail\n");
                System.exit(1);
            }
            bs = toLSB(i8);
            if(i8 != LSB2Long(bs)) {
                System.err.println("toLSB or LSB2Long fail\n");
                System.exit(1);
            }

            // test f8
            bs = toMSB(f8);
            if(f8 != MSB2Double(bs)) {
                System.err.println("toMSB or MSB2Double fail\n");
                System.exit(1);
            }
            bs = toLSB(f8);
            if(f8 != LSB2Double(bs)) {
                System.err.println("toLSB or LSB2Double fail\n");
                System.exit(1);
            }

            // System.out.println("Test " + (i + 1) + " success");
        }

        System.out.println("Pass " + i + " tests");
    }

    public static byte[] toMSB(short i) {
        byte[] val = new byte[2];
        val[0] = (byte) (i >> 8);
        val[1] = (byte) i;
        return val;
    }

    public static byte[] toLSB(short i) {
        byte[] val = new byte[2];
        val[0] = (byte) i;
        val[1] = (byte) (i >> 8);
        return val;
    }

    public static byte[] toMSB(int i) {
        byte[] val = new byte[4];
        val[0] = (byte) (i >> 24);
        val[1] = (byte) (i >> 16);
        val[2] = (byte) (i >> 8);
        val[3] = (byte) i;
        return val;
    }

    public static byte[] toLSB(int i) {
        byte[] val = new byte[4];
        val[0] = (byte) i;
        val[1] = (byte) (i >> 8);
        val[2] = (byte) (i >> 16);
        val[3] = (byte) (i >> 24);
        return val;
    }

    public static byte[] toMSB(float f) {
        return toMSB(Float.floatToRawIntBits(f));
    }

    public static byte[] toLSB(float f) {
        return toLSB(Float.floatToRawIntBits(f));
    }

    public static byte[] toMSB(long i) {
        byte[] val = new byte[8];
        int ih = (int) (i >> 32), il = (int)i;
        val[0] = (byte) (ih >> 24);
        val[1] = (byte) (ih >> 16);
        val[2] = (byte) (ih >> 8);
        val[3] = (byte) ih;
        val[4] = (byte) (il >> 24);
        val[5] = (byte) (il >> 16);
        val[6] = (byte) (il >> 8);
        val[7] = (byte) il;
        return val;
    }

    public static byte[] toLSB(long i) {
        byte[] val = new byte[8];
        int ih = (int) (i >> 32), il = (int)i;
        val[0] = (byte) il;
        val[1] = (byte) (il >> 8);
        val[2] = (byte) (il >> 16);
        val[3] = (byte) (il >> 24);
        val[4] = (byte) ih;
        val[5] = (byte) (ih >> 8);
        val[6] = (byte) (ih >> 16);
        val[7] = (byte) (ih >> 24);
        return val;
    }

    public static byte[] toMSB(double f) {
        return toMSB(Double.doubleToRawLongBits(f));
    }

    public static byte[] toLSB(double f) {
        return toLSB(Double.doubleToRawLongBits(f));
    }

    public static short MSB2Short(byte[] bs) {
        return (short)((bs[0] << 8) | (bs[1] & 0xFF));
    }

    public static short LSB2Short(byte[] bs) {
        return (short)((bs[1] << 8) | (bs[0] & 0xFF));
    }

    public static int LSB2Int(byte[] bs) {
        int val = bs[0] & 0xFF;
        val |= (bs[1] & 0xFF) << 8;
        val |= (bs[2] & 0xFF) << 16;
        val |= bs[3] << 24;
        return val;
    }

    public static int MSB2Int(byte[] bs) {
        int val = bs[3] & 0xFF;
        val |= (bs[2] & 0xFF) << 8;
        val |= (bs[1] & 0xFF) << 16;
        val |= bs[0] << 24;
        return val;
    }

    public static float LSB2Float(byte[] bs) {
        return Float.intBitsToFloat(LSB2Int(bs));
    }

    public static float MSB2Float(byte[] bs) {
        return Float.intBitsToFloat(MSB2Int(bs));
    }

    public static long LSB2Long(byte[] bs) {
        int ih = bs[4] & 0xFF, il = bs[0] & 0xFF;
        ih |= (bs[5] & 0xFF) << 8;
        ih |= (bs[6] & 0xFF) << 16;
        ih |= bs[7] << 24;
        il |= (bs[1] & 0xFF) << 8;
        il |= (bs[2] & 0xFF) << 16;
        il |= bs[3] << 24;
        return (long) ih << 32 | (il & 0xFFFFFFFFL);
    }

    public static long MSB2Long(byte[] bs) {
        int ih = bs[3] & 0xFF, il = bs[7] & 0xFF;
        ih |= (bs[2] & 0xFF) << 8;
        ih |= (bs[1] & 0xFF) << 16;
        ih |= bs[0] << 24;
        il |= (bs[6] & 0xFF) << 8;
        il |= (bs[5] & 0xFF) << 16;
        il |= bs[4] << 24;
        return (long) ih << 32 | (il & 0xFFFFFFFFL);
    }

    public static double LSB2Double(byte[] bs) {
        return Double.longBitsToDouble(LSB2Long(bs));
    }

    public static double MSB2Double(byte[] bs) {
        return Double.longBitsToDouble(MSB2Long(bs));
    }
}
