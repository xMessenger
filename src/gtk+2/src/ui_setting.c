#include "gtk2-ui.h"

GtkWidget * create_setting_page()
{
    GtkWidget *frame;

    /* 设置所有组件都在此容器中 */
    frame = gtk_frame_new(_("TBD"));
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    gtk_container_add(GTK_CONTAINER(frame), gtk_label_new(_("TBD")));

    return frame;
}
