#include "gtk2-ui.h"

GtkWidget *g_logger_view;

static void insert_text(GtkTextBuffer *buffer)
{
    GtkTextIter iter;

    gtk_text_buffer_get_iter_at_offset(buffer, &iter, 0);

    gtk_text_buffer_insert(buffer, &iter,
        "From: pathfinder@nasa.gov\n"
        "To: mom@nasa.gov\n"
        "Subject: Made it!\n"
        "\n"
        "We just got in this morning. The weather has been\n"
        "great - clear but cold, and there are lots of fun sights.\n"
        "Sojourner says hi. See you soon.\n"
        " -Path\n", -1);
}

/* Create a scrolled text area that displays a "message" */
static GtkWidget *create_text(void)
{
    GtkWidget *scrolled_window;
    GtkTextBuffer *buffer;

    g_logger_view = gtk_text_view_new();
    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(g_logger_view));

    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_container_add(GTK_CONTAINER(scrolled_window), g_logger_view);
    insert_text(buffer);

    gtk_widget_show_all(scrolled_window);

    return scrolled_window;
}

GtkWidget * create_logger_page()
{
    GtkWidget *frame, *text;

    /* 最新消息记录到此frame中 */
    frame = gtk_frame_new("最新消息");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    gtk_widget_set_size_request(frame, 100, 75);
    gtk_widget_show(frame);
    text = create_text();
    gtk_container_set_border_width(GTK_CONTAINER(text), 5);
    gtk_container_add(GTK_CONTAINER(frame), text);

    return frame;
}
