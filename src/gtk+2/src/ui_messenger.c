#include "gtk2-ui.h"

static void insert_text(GtkTextBuffer *buffer)
{
    GtkTextIter iter;

    gtk_text_buffer_get_iter_at_offset(buffer, &iter, 0);

    gtk_text_buffer_insert(buffer, &iter,
        "From: pathfinder@nasa.gov\n"
        "To: mom@nasa.gov\n"
        "Subject: Made it!\n"
        "\n"
        "We just got in this morning. The weather has been\n"
        "great - clear but cold, and there are lots of fun sights.\n"
        "Sojourner says hi. See you soon.\n"
        " -Path\n", -1);
}

/* Create a scrolled text area that displays a "message" */
static GtkWidget *create_text(void)
{
    GtkWidget *scrolled_window;
    GtkWidget *view;
    GtkTextBuffer *buffer;

    view = gtk_text_view_new();
    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));

    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
            GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_container_add(GTK_CONTAINER(scrolled_window), view);
    insert_text(buffer);

    gtk_widget_show_all(scrolled_window);

    return scrolled_window;
}

/* Create the list of "messages" */
static GtkWidget *create_list(void)
{
    GtkWidget *scrolled_window;
    GtkWidget *tree_view;
    GtkListStore *model;
    GtkTreeIter iter;
    GtkCellRenderer *cell;
    GtkTreeViewColumn *column;

    int i;

    /* Create a new scrolled window, with scrollbars only if needed */
    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
                    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    model = gtk_list_store_new(3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    tree_view = gtk_tree_view_new();
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), tree_view);
    gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view), GTK_TREE_MODEL(model));
    gtk_widget_show(tree_view);

    /* Add some messages to the window */
    for(i = 0; i < 10; i++) {
        gchar *msg = g_strdup_printf("user%02d", i);
        gchar *grp = g_strdup_printf("group%02d", i);
        gchar *val = g_strdup_printf("192.168.0.%d", i + 1);
        gtk_list_store_append(GTK_LIST_STORE(model), &iter);
        gtk_list_store_set(GTK_LIST_STORE(model), &iter, 0, msg, -1);
        gtk_list_store_set(GTK_LIST_STORE(model), &iter, 1, grp, -1);
        gtk_list_store_set(GTK_LIST_STORE(model), &iter, 2, val, -1);

        g_free(val);
        g_free(grp);
        g_free(msg);
    }

    cell = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("用户", cell, "text", 0, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), GTK_TREE_VIEW_COLUMN(column));

    column = gtk_tree_view_column_new_with_attributes("组别", cell, "text", 1, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), GTK_TREE_VIEW_COLUMN(column));

    column = gtk_tree_view_column_new_with_attributes("IP", cell, "text", 2, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), GTK_TREE_VIEW_COLUMN(column));

    return scrolled_window;
}

GtkWidget * create_messenger_page()
{
    GtkWidget *box, *frame, *vbx, *hbx, *label, *button, *list, *text;

    /* 消息页面的所有组件都在此box中 */
    box = gtk_vbox_new (FALSE, 0);
    gtk_widget_show(box);

    /* 用户信息frame */
    frame = gtk_frame_new("用户");
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    gtk_widget_set_size_request(frame, 450, 280);
    gtk_widget_show(frame);

    vbx = gtk_vbox_new(FALSE, 1);

    hbx = gtk_hbutton_box_new();
    gtk_container_set_border_width(GTK_CONTAINER(hbx), 5);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(hbx), GTK_BUTTONBOX_EDGE);
    gtk_box_set_spacing(GTK_BOX(hbx), 5);

    label = gtk_label_new("在线用户27人");
    gtk_container_add(GTK_CONTAINER(hbx), label);

    button = gtk_button_new_with_label ("刷新用户列表");
    gtk_container_add(GTK_CONTAINER(hbx), button);

    gtk_box_pack_start (GTK_BOX (vbx), hbx, FALSE, FALSE, 0);

    list = create_list();
    gtk_container_set_border_width(GTK_CONTAINER(list), 5);
    gtk_widget_set_size_request(GTK_WIDGET(list), 450, 215);
    gtk_widget_show(list);

    gtk_box_pack_start (GTK_BOX (vbx), list, FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(frame), vbx);

    gtk_widget_show_all (frame);

    gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);
    gtk_widget_show_all (box);
    /*  TODO: 当notebook高度改变时，修改其高度  */
    frame = gtk_frame_new(_("message"));
    gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
    gtk_widget_set_size_request(frame, 450, 180);
    gtk_widget_show(frame);
    text = create_text();
    gtk_container_set_border_width(GTK_CONTAINER(text), 5);
    gtk_container_add(GTK_CONTAINER(frame), text);
    gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);


    hbx = gtk_hbutton_box_new();
    gtk_container_set_border_width(GTK_CONTAINER(hbx), 5);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(hbx), GTK_BUTTONBOX_SPREAD);
    gtk_box_set_spacing(GTK_BOX(hbx), 5);

    label = gtk_check_button_new_with_label("封装");
    gtk_container_add(GTK_CONTAINER(hbx), label);
    label = gtk_check_button_new_with_label("广播");
    gtk_container_add(GTK_CONTAINER(hbx), label);
    label = gtk_check_button_new_with_label("加密");
    gtk_container_add(GTK_CONTAINER(hbx), label);

    button = gtk_button_new_with_label ("发送");
    gtk_container_add(GTK_CONTAINER(hbx), button);

    gtk_box_pack_start (GTK_BOX (box), hbx, FALSE, FALSE, 0);

    return box;
}
