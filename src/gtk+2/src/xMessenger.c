/*
 * xMessenger 主文件
 *
 * gcc -O2 -march=i686 -o xMessenger xMessenger.c -Wall `pkg-config gtk+-2.0 --cflags --libs`
 * gcc -O2 -march=i686 -o xMessenger xMessenger.c -mwindows -Wall `pkg-config gtk+-2.0 --cflags --libs`
 *
 * @author <a href="mailto:egcs@21cn.com">Dongsheng Song</a>
 */
#include <stdio.h>
#include <gtk/gtk.h>

#include "gtk2-ui.h"
int main(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *notebook, *box;

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_widget_set_size_request(window, 450, 540);
    gtk_window_set_title(GTK_WINDOW(window), _("xMessenger - 27 users online"));
    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(gtk_main_quit), NULL);
    gtk_container_set_border_width(GTK_CONTAINER(window), 5);

    /* 所有页面都在此notebook中 */
    notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
    gtk_container_add(GTK_CONTAINER(window), notebook);

    /* 将消息页面加入notebook中 */
    box = create_messenger_page();
    gtk_widget_show_all (box);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, gtk_label_new(_("message")));


    /* 将日志页面加入notebook中 */
    box = create_logger_page();
    gtk_widget_show_all(box);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, gtk_label_new(_("log")));


    /* 将设置页面加入notebook中 */
    box = create_setting_page();
    gtk_widget_show_all(box);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, gtk_label_new(_("setting")));


    /* 将关于页面加入notebook中 */
    /*
        box = create_about_page();
        gtk_widget_show_all(box);
        gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, gtk_label_new(_("about")));
    */

    /* Set what page to start */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0);

    gtk_widget_show(notebook);
    gtk_widget_show(window);

    gtk_main();

    return 0;
}
