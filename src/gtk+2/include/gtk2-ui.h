/*
 * GTK2 UI 定义
 */
#ifndef GTK2_UI_H
#define GTK2_UI_H

#include <gtk/gtk.h>

#include "sysdep.h"

GtkWidget * create_messenger_page();
GtkWidget * create_logger_page();
GtkWidget * create_setting_page();
GtkWidget * create_about_page();

extern GtkWidget *g_logger_view;

#endif /* GTK2_UI_H */
