/*
 * 系统相关定义
 */
#ifndef SYSDEP_H
#define SYSDEP_H

#ifdef ENABLE_NLS
    #include <libintl.h>
    #define _(String) dgettext (PACKAGE, String)
#else
    # define _(String) (String)
#endif

#endif /* ! defined (SYSDEP_H) */
