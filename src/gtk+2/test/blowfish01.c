/*
 * blowfish算法测试
 */

#include <stdio.h>
#include <stdlib.h>

#include "blowfish.h"

#define die(msg) { perror(msg); exit(EXIT_FAILURE); }

int main(int argc, char *argv[])
{
    unsigned long L = 1, R = 2;
    BLOWFISH_CTX ctx;

    Blowfish_Init (&ctx, (unsigned char*)"TESTKEY", 7);
    Blowfish_Encrypt(&ctx, &L, &R);

    printf("%08lX %08lX\n", L, R);

    if (L == 0xDF333FD2L && R == 0x30A71BB4L)
        printf("Test encryption OK.\n");
    else
        die("Test encryption failed.\n");

    Blowfish_Decrypt(&ctx, &L, &R);

    if (L == 1 && R == 2)
        printf("Test decryption OK.\n");
    else
        die("Test decryption failed.\n");
}
