/*
 * 多播测试 - 客户机
 *
 * You must rebuild your Linux kernel to support Multicast IP
 * and you have to issue something like this (as root):
 *   route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#define die(msg) { perror(msg); exit(EXIT_FAILURE); }

char * host_name = "224.0.0.1"; // local host (multicast IP)
const int port = 6789;

int main(int argc, char *argv[])
{
    struct ip_mreq command;
    u_char loop = 1;  // we want broadcast to also loop back to this computer
    int iter = 0;
    int sin_len;
    char message[256];
    int socket_descriptor;
    struct sockaddr_in sin;

    char * str = "A default test string";

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(port);

    if ((socket_descriptor = socket(PF_INET, SOCK_DGRAM, 0)) == -1)
        die("socket");

    /* allow multiple processes to use this same port */
    /*
    loop = 1;
    if (setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &loop, sizeof(loop)) < 0)
        die("setsockopt(allow multiple socket use)");
    */

    if (bind(socket_descriptor, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        die("bind");

    /* allow broadcast to this machine */
    loop = 1;
    if (setsockopt(socket_descriptor, IPPROTO_IP, IP_MULTICAST_LOOP,
            &loop, sizeof(loop)) < 0)
        die("setsockopt(multicast loop on");

    /* join the broadcast group */
    command.imr_multiaddr.s_addr = inet_addr("224.0.0.1");
    command.imr_interface.s_addr = htonl(INADDR_ANY);
    if (command.imr_multiaddr.s_addr == -1)
        die("group of 224.0.0.1 not a legal multicast address");

    if (setsockopt(socket_descriptor, IPPROTO_IP, IP_ADD_MEMBERSHIP, &command, sizeof(command)) < 0)
        die("setsocket(add membership)");

    while (iter++ < 100) {
        sin_len = sizeof(sin);
        memset(&message, 0, sizeof(message));
        if (recvfrom(socket_descriptor, message, 256, 0,
                (struct sockaddr *)&sin, &sin_len) == -1) {
            perror("Error in receiving response from server\n");
        } else {
            printf("\nResponse from server:\n\t%s\n", message);
        }
        sleep(1);
    }

    /* leave the broadcast group ('struct ip_mreq command' is still setup OK) */
    if (setsockopt(socket_descriptor, IPPROTO_IP, IP_DROP_MEMBERSHIP,
            &command, sizeof(command)) < 0)
        die("setsocket(drop membership)");

    close(socket_descriptor);

    return 0;
}
