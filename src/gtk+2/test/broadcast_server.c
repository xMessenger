/*
 * 多播测试 - 服务器
 *
 * You must rebuild your Linux kernel to support Multicast IP
 * and you have to issue something like this (as root):
 *   route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#define die(msg) { perror(msg); exit(EXIT_FAILURE); }

const int port = 6789;


int main(int argc, char *argv[])
{
    int socket_descriptor, count = 0;
    struct sockaddr_in address;
    char *fmt = "[%d] test from broadcast";
    char buffer[256];

    /* For broadcasting, this socket can be treated like a UDP socket: */
    socket_descriptor = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_descriptor == -1)
        die("socket");

    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr("224.0.0.1");
    address.sin_port = htons(port);

    while (count < 1000) {
        count++;
        sprintf(buffer, fmt, count);
        if (sendto(socket_descriptor, buffer, strlen(buffer), 0,
                (struct sockaddr *)&address, sizeof(address)) < 0)
            die("sendto");
        sleep(3);
    }

    close(socket_descriptor);

    return 0;
}
