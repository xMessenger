/* Automatically generated by po2tbl.sed from gipmsg.pot.  */

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include "libgettext.h"

const struct _msg_ent _msg_tbl[] = {
  {"", 1},
  {"geometry of TOP window", 2},
  {"username", 3},
  {"nickname", 4},
  {"IP Messenger port", 5},
  {"broadcast address of local network", 6},
  {" - user shortcut %d - ", 7},
  {"Main", 8},
  {"Absence", 9},
  {"Broadcast", 10},
  {"Sound", 11},
  {"Can not view file.", 12},
  {"OK", 13},
  {"H.Shirouzu", 14},
  {"T.Matsumaru", 15},
  {"\
IP Messenger for X Window\n\
\n\
UDP based communication application over LAN", 16},
  {"cannot find hostname\n", 17},
  {"inet_aton: broadcast address[%s] cannot convert to binary.\n", 18},
  {"socket alloc failed.\n", 19},
  {"could not bind.\n", 20},
  {"Sun", 21},
  {"Mon", 22},
  {"Tue", 23},
  {"Wed", 24},
  {"Thu", 25},
  {"Fri", 26},
  {"Sat", 27},
  {"Jan", 28},
  {"Feb", 29},
  {"Mar", 30},
  {"Apr", 31},
  {"May", 32},
  {"Jun", 33},
  {"Jul", 34},
  {"Aug", 35},
  {"Sep", 36},
  {"Oct", 37},
  {"Nov", 38},
  {"Dec", 39},
  {"Broadcast From...", 40},
  {"Multicast From...", 41},
  {"%s[%s] (%s) - gipmsg", 42},
  {"\
%s[%s] (%s)\n\
opened\n\
  (at %s %2u %2u:%02u)  ", 43},
  {"User num", 44},
  {"IPMSG_RECVMSG: unknown message", 45},
  {"illegal version of ipmsg protocol", 46},
  {"\
GIPMSG - port %u\n\
 %d users logged on\n\
 You've %d messages", 47},
  {"\
GIPMSG - port %u\n\
 %d users logged on", 48},
  {"Send Message", 49},
  {"Nick", 50},
  {"User", 51},
  {"Machine", 52},
  {"Group", 53},
  {"IP Address", 54},
  {"Refresh", 55},
  {"Send", 56},
  {"Seal", 57},
  {"Lock", 58},
  {"Receive Message", 59},
  {"Message From...", 60},
  {"Open", 61},
  {"Close", 62},
  {"Reply", 63},
  {"Cite", 64},
  {"< cancel >", 65},
  {"Couldn't create pixmap", 66},
  {"About...", 67},
  {"Properties...", 68},
  {"Show Log", 69},
  {"Properties of this program", 70},
  {"show log", 71},
  {"Info about this program", 72},
  {"Quit...", 73},
  {"Quit from program", 74},
  {"Save position", 75},
  {"Save posiion", 76},
  {"Forget position", 77},
  {"Forget posiion", 78},
  {"Save width & height", 79},
  {"Forget width & height", 80},
  {"retry", 81},
  {"cancel", 82},
  {"GIPMSG - resend?", 83},
  {"\
Cannot send message to %s(%s).\n\
 Do you retry?", 84},
  {"Away", 85},
  {"I'm away, now.", 86},
  {"Lunch", 87},
  {"I am getting lunch.", 88},
  {"Conference", 89},
  {"conference", 90},
  {"Visitor", 91},
  {"I've got visitor.", 92},
  {"Gone", 93},
  {"i've get out now.", 94},
  {"Homecoming", 95},
  {"homecoming", 96},
  {"ramble", 97},
  {"Rambling Rambling Rambling", 98},
  {"hehe", 99},
  {"BowWow!!", 100},
  {"Nickname", 101},
  {"Option", 102},
  {"check opened seal", 103},
  {"non-popup message", 104},
  {"default use seal", 105},
  {"default use cite", 106},
  {"Log", 107},
  {"logging", 108},
  {"Select Log file", 109},
  {"Broadcast for out of Local Network", 110},
  {"(ex. 192.168.128.255)", 111},
  {"add", 112},
  {"remove", 113},
  {"Absence Messages", 114},
  {"No", 115},
  {"Title", 116},
  {"Message", 117},
  {"Change", 118},
  {"beep", 119},
  {"Play Sound", 120},
  {"Select Sound file", 121},
  {"Play", 122},
};

int _msg_tbl_length = 122;
