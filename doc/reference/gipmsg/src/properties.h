#ifndef PROPERTIES_H__
#define PROPERTIES_H__


#define IPMSG_ABSENCE_NUM		(8)
#define	SHORTCUT_USER_NUM		(10)

struct OPTION_T {
	char	*user;
	char	*nick;
	char	*group;
	char	*geometry;
	guint16	port;
	char	*localnet_bcast;
	char	*broadcasts;
	int		applet;

	int		default_sort_column;
	int		default_sort_type;

	char	*absence[IPMSG_ABSENCE_NUM];
	char	*absence_text[IPMSG_ABSENCE_NUM];

	char	*shortcut_user[SHORTCUT_USER_NUM];
	
	unsigned		check_seal		:1;
	unsigned		non_popup		:1;
	unsigned		seal			:1;
	unsigned		cite			:1;
	unsigned		log				:1;
	char	*log_fname;
	unsigned		beep			:1;
	unsigned		snd				:1;
	char	*snd_fname;
};


void load_properties(char *path, struct OPTION_T *prop );
void save_properties(char *path, struct OPTION_T *prop );

void on_prop_apply(GnomePropertyBox *propertybox,
				   gint arg1,
				   gpointer user_data);

GtkWidget* create_prop_main(GtkWidget *widget);
GtkWidget* create_prop_broadcast(GtkWidget *widget);
GtkWidget* create_prop_absence(GtkWidget *widget);
GtkWidget* create_prop_sound (GtkWidget *widget);



#ifndef PROPERTIES_C__
extern struct OPTION_T option;
#endif /* PROPERTIES_C__ */


#endif /* PROPERTIES_H__ */
