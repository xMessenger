/*
 * A simple Gnome program, outside of GNOME tree, not using i18n
 * buttons.c
 */
/* the very basic gnome include */

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#define	MAIN_C__

#include <gnome.h>
#include <signal.h>
#ifdef APPLET
#  include <applet-widget.h>
#endif
#include "properties.h"
#include "win.h"
#include "ipmsg.h"
#include "sig.h"
#include "main.h"

#ifdef APPLET
gboolean is_applet = FALSE;
#endif

#ifdef SUNOS41X
static int
atexit(void (*proc)(void))
{
	return on_exit((void (*)(int, caddr_t))proc, NULL);
}
#endif

#ifdef NO_ATEXIT
static void (*at_exit_proc)(void);
static void inttrap(void)
{
	if (at_exit_proc != NULL) {
		at_exit_proc();
	}
	exit(1);
}

static int atexit(void (*proc)(void))
{
	at_exit_proc = proc;
	signal(SIGINT, inttrap);
	signal(SIGHUP, inttrap);
	signal(SIGTERM, inttrap);
	return 0;
}
#endif

static void exit_proc(void)
{
	IPMsgQuit();
}

static void kill_exit_proc(int dummy)
{
	alarm(0);
	exit(EXIT_SUCCESS);
}

static void trap_usr1(int dummy)
{
	IPMsgOpenSendWin();
}

static struct poptOption popt_options[] = {
	{"geometry", '\0', POPT_ARG_STRING, &option.geometry, 0, N_("geometry of TOP window"), "28x28+0-0"},
	{"user", 'u', POPT_ARG_STRING, &option.user, 0, N_("username"), "USER"},
//	{"nick", 'n', POPT_ARG_STRING, &option.nick, 0, N_("nickname"), "NICK"},
	{"port", 'p', POPT_ARG_INT, &option.port, 0, N_("IP Messenger port"), "PORT"},
	{"broadcast", '\0', POPT_ARG_STRING, &option.localnet_bcast, 0, N_("broadcast address of local network"), "255.255.255.255"},
	{NULL, '\0', 0, NULL, 0} /* end the list */
};


/*================================================================
 *	GNOME session management
 *================================================================*/
#ifdef APPLET
static gint save_session(GtkWidget *widget, char *privcfgpath,
						 char *globcfgpath, gpointer data)
{
	save_properties(privcfgpath,&option);
	return FALSE;
}
#endif /* APPLET */

/* shamelessly stolen from http://www.gnome.org/devel/start/sm.shtml 
*/


/* nothing spectacular here - does it need to do anything else?? */
void
main_quit (void)
{
#ifdef APPLET
	if (applet_p())
		applet_widget_gtk_main_quit();
	else
#endif /* APPLET */
		gtk_main_quit();
}

static void
session_die (gpointer client_data)
{
	IPMsgQuit();
	main_quit();
}

static int
save_state (GnomeClient        *client,
			gint                phase,
			GnomeRestartStyle   save_style,
			gint                shutdown,
			GnomeInteractStyle  interact_style,
			gint                fast,
			gpointer            client_data)
{
	save_properties("gipmsg/",&option);
	return TRUE;
}


GnomeClient *newGnomeClient()
{
	gchar buf[1024];

	GnomeClient *client;

	client = gnome_client_new();

	if (!client)
		return NULL;

	getcwd((char *)buf,sizeof(buf));
	gnome_client_set_current_directory(client, (char *)buf);

	gtk_object_ref(GTK_OBJECT(client));
	gtk_object_sink(GTK_OBJECT(client));
	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
						GTK_SIGNAL_FUNC (save_state), NULL);
	gtk_signal_connect (GTK_OBJECT (client), "die",
						GTK_SIGNAL_FUNC (session_die), NULL);
	return client;
}


/*
 */

int main(int argc, char *argv[])
{
    GnomeClient *smClient;
	struct {
		gint	x, y, h, w;
	} geom = { -1, -1, -1, -1 };
	GtkWidget *app;

#ifdef APPLET
	{
		static const char applet[] = "_applet";
		size_t len = strlen(argv[0]);
		if (len > sizeof(applet) &&
		    strcmp(argv[0] + len - sizeof(applet) + 1, applet) == 0)
			is_applet = TRUE;
	}
#endif /* APPLET */

	//malloc(2048);				/* for debug */
	
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

#ifdef APPLET
	if (applet_p()) {
		applet_widget_init(PACKAGE, VERSION, argc, argv, popt_options, 0, NULL);
		winMain = app = applet_widget_new(PACKAGE);
	} else
#endif /* APPLET */
	{
		gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, popt_options, 0, NULL);
		winMain = app = gnome_app_new (PACKAGE, "GIPMSG");
	}
	smClient = newGnomeClient(); /* initialize session management */
	gtk_signal_connect (GTK_OBJECT (app), "destroy",
						GTK_SIGNAL_FUNC (on_buttonWin_destroy),
						NULL);
	if (option.user == NULL)	option.user = getenv("USER");
	if (option.user == NULL)	option.user = "anonymous";
	//load_properties(APPLET_WIDGET(app)->privcfgpath, &option);
	load_properties("gipmsg/", &option);
	IPMsgSetup(option.port);
	create_buttonWin(app);
#ifdef APPLET
	if (applet_p()) {
		gtk_signal_connect(GTK_OBJECT(app),"save_session",
						   GTK_SIGNAL_FUNC(save_session),
						   NULL);
		gtk_widget_set_usize(app, 24, 24);
	} else
#endif /* APPLET */
	{
		if (option.geometry != NULL) {
			gnome_parse_geometry(option.geometry, &geom.x, &geom.y, &geom.w, &geom.h);
			gtk_widget_set_uposition(app, geom.x, geom.y);
		}
		if (geom.w == -1)	geom.w = 32;
		if (geom.h == -1)	geom.h = 32;
		gtk_widget_set_usize(app, geom.w, geom.h);
	}
	gtk_widget_show_all(app);
	gtk_widget_hide(get_widget(GTK_WIDGET(app), "buttonWinPix2"));

	IPMsgLogin();

	signal(SIGUSR1, trap_usr1);
	signal(SIGTERM, kill_exit_proc);
	atexit(exit_proc);

#ifdef APPLET
	if (applet_p())
		applet_widget_gtk_main();
	else
#endif /* APPLET */
		gtk_main();
	return 0;
}
