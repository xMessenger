/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  gipmsg
 *  Copyright (C) <YEAR> <AUTHORS>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#define	SIG_C__

#include <gnome.h>
#ifdef APPLET
#  include <applet-widget.h>
#endif
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include "properties.h"
#include "win.h"
#include "sig.h"
#include "ipmsg.h"
#include "main.h"

GtkWidget	*winMain;
GtkWidget	*winSend;
GtkWidget	*winRecv;
GtkWidget	*winAbout;
GtkWidget	*winLog;
GtkWidget	*menu_select_group_sendUserList;


struct st_widget_destroy {
	guint		tag;
	GtkWidget	*widget;
};
gint widget_destroy(gpointer data)
{
	struct st_widget_destroy	*st = data;
	gtk_timeout_remove(st->tag);
	gtk_widget_destroy(st->widget);
	g_free(st);
	return TRUE;
}


void
on_sendWin_destroy                     (GtkObject       *object,
                                        gpointer         user_data)
{
	if ((void*)object == (void*)winSend)	winSend = 0;
}


gboolean
on_sendWin_key_press_event      (GtkWidget       *widget,
								 GdkEventKey     *event,
								 gpointer         user_data)
{
	IPMsgList	*list;
	gint		row;
	gchar		*nicks;
	gchar		*toks;
	gchar		*nick;
	GString		*gstr;
	GString		*gstr_unmark;
	GtkWidget	*win;
	GtkWidget	*tips;
	struct st_widget_destroy	*stwd;
	GtkWidget	*gwin;
	gint		gx, gy;

	//g_print("sendWin: key press: %u, %d\n", event->state, event->keyval);
	/* with control key */
	switch (event->keyval) {
	case 48:					/* 0 */
	case 49:					/* 1 */
	case 50:					/* 2 */
	case 51:					/* 3 */
	case 52:					/* 4 */
	case 53:					/* 5 */
	case 54:					/* 6 */
	case 55:					/* 7 */
	case 56:					/* 8 */
	case 57:					/* 9 */
		if ( (event->state & GDK_CONTROL_MASK) ) { /* with control key */
			if ( !(event->state & GDK_MOD1_MASK) ) /* without mod key */
				gtk_clist_unselect_all(GTK_CLIST(user_data));
			gstr = g_string_new ("");
			g_string_sprintf (gstr, _(" - user shortcut %d - "), event->keyval - 48);
			gstr_unmark = g_string_new ("\n-----");
			toks = nicks = g_strdup(option.shortcut_user[event->keyval - 48]);
			while ((nick = strtok(toks, ":")) != NULL) {
				toks = NULL;
				if ((list = (IPMsgList*)db_foreach(db, IPMsgSearchNick, nick)) != 0) {
					row = gtk_clist_find_row_from_data( GTK_CLIST (user_data), list);
					gtk_clist_select_row(GTK_CLIST(user_data), row, 0);
					if (gtk_clist_row_is_visible (GTK_CLIST(user_data), row) != GTK_VISIBILITY_FULL) {
						gtk_clist_moveto(GTK_CLIST(user_data), row, 0, 0.0, 0.0);
					}
					g_string_sprintfa (gstr, "\n* %s", nick);
				}
				else {
					g_string_sprintfa (gstr_unmark, "\n  %s", nick);
				}
			}
			win = gtk_window_new(GTK_WINDOW_POPUP);
			g_string_append(gstr, gstr_unmark->str);
			tips = gtk_label_new (gstr->str);
			gtk_container_add (GTK_CONTAINER (win), tips);
			gtk_widget_show (tips);
			gwin = get_widget_window(GTK_WIDGET(user_data));
			gdk_window_get_position(gwin->window, &gx, &gy);
			gtk_widget_set_uposition(win, gx, gy);
			gtk_widget_show (win);
			g_string_free(gstr_unmark, TRUE);
			g_string_free(gstr, TRUE);
			stwd = g_malloc(sizeof(struct st_widget_destroy));
			stwd->widget = win;
			stwd->tag = gtk_timeout_add(2000, widget_destroy, stwd);
		}
		break;
	}
	return TRUE;
}


gboolean
on_sendWinEv_button_press_event      (GtkWidget       *widget,
									GdkEventButton  *event,
									gpointer         user_data)
{
	switch (event->button) {
	case 3:						/*  */
		g_print("button 3 clicked\n");
#if 0
@@		gnome_popup_menu_do_popup(create_geometry_menu(), NULL, NULL, event, NULL);
#endif
		break;
	}
	return TRUE;
}


void
on_sendRefreshB_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "sendDohoTB")), 0);
	gtk_clist_clear(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "sendUserList")));
	db_foreach(db, IPMsgDBEachFree_list, NULL);
	db_clear(db);
	IPMsgSendBroadcast(IPMSG_BR_ENTRY, IPMsgGetNickWithAbsence());
	winSend = user_data;
}


void
on_sendSendB_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget*	textbox;
	GtkCList	*clist;
	GList		*p;
	gpointer	data;
	int		i;
	guint32		flag;
	GtkToggleButton	*tb;

	textbox = get_widget(GTK_WIDGET(user_data), "sendTextBox");
	if (gtk_text_get_length(GTK_TEXT(textbox)) <= 0)	return;
	clist = (GtkCList*)get_widget(GTK_WIDGET(user_data), "sendUserList");
	for (p = clist->selection, i = 0; p; p = p->next) {
		i++;
		if (p == clist->selection_end)	break;
	}
	flag = 0;
	tb = GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "sendDohoTB"));
	if (i == 0 && tb->active == 0)	return;
	if (tb->active) {
		IPMsgSendDataBroadcast(user_data, IPMSG_BROADCASTOPT);
	}
	else {
		if (i > 1)		flag = IPMSG_MULTICASTOPT;
		for (p = clist->selection; p; p = p->next) {
			data = gtk_clist_get_row_data(clist, (gint)p->data);
			IPMsgSendData(data, user_data, flag);
			if (p == clist->selection_end)	break;
		}
	}
	gtk_widget_destroy(user_data);
}


void
on_recvSend_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{
	IPMsgReadMsgData	*rmsg;
	IPMsgList		*list;
	GtkWidget *sendWin;
	GtkToggleButton	*t_button;
	GtkWidget	*sendText;
#define	INSERT_BUFF_SIZE	1024
	gchar		insert_buff[INSERT_BUFF_SIZE+1];
	gchar		*pStr;
	gchar		*pdup;
	GtkCList	*clist;
	gint		clist_row;

	if ((rmsg = (IPMsgReadMsgData*)db_foreach(db_read, IPMsgSearchReadMsg, user_data)) != 0) {
		sendWin = IPMsgOpenSendWin();
		if ((list = (IPMsgList*)db_foreach(db, IPMsgSearchReadFrom, rmsg)) != 0) {
			clist = GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList"));
			clist_row = gtk_clist_find_row_from_data(clist, list);
			gtk_clist_select_row(clist, clist_row, 0);
			if (gtk_clist_row_is_visible (clist, clist_row) != GTK_VISIBILITY_FULL) {
				gtk_clist_moveto(clist, clist_row, 0, 0.0, 0.0);
			}
			t_button = GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "recvWinCite"));
			if (t_button->active != 0) {
				sendText = get_widget(GTK_WIDGET(sendWin), "sendTextBox");
				gtk_text_freeze(GTK_TEXT(sendText));
				pdup = g_strdup(rmsg->msg);
				pStr = strtok(pdup, "\n");
				do {
					sprintf(insert_buff, ">%s\n", pStr);
					gtk_text_insert(GTK_TEXT(sendText), NULL, NULL, NULL,
									insert_buff, -1);
				} while ((pStr = strtok(NULL, "\n")) != NULL);
				gtk_text_thaw (GTK_TEXT (sendText));
				g_free(pdup);
			}
		}
	}
}


void
on_recvWinKaifu_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
	gtk_widget_hide(get_widget(GTK_WIDGET(user_data), "recvWinKaifu"));
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(user_data), "recvWinKaifu")), FALSE);
	gtk_widget_show(get_widget(GTK_WIDGET(user_data), "vpaned1"));
	gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvWinScrollbar"));
	gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvText"));
	//gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvCloseB"));
	//gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvSendB"));
	//gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvWinCite"));
	gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvWinButtonBox"));
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(user_data), "recvCloseB")), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(user_data), "recvSendB")), TRUE);
	gtk_widget_grab_focus(get_widget(GTK_WIDGET(user_data), "recvText")); /* focus��ܤ� */
}

void
on_recvWinKaifu_clicked_2                (GtkButton       *button,
                                        gpointer         user_data)
{
	gchar	buf[32];
	IPMsgReadMsgData	*rmsg;

	rmsg = user_data;
	sprintf(buf, "%u", rmsg->serial);
	IPMsgSend(&rmsg->from, IPMSG_READMSG, &buf[0]);
}

void
on_recvWinKaifu_clicked_3                (GtkButton       *button,
										  gpointer         user_data)
{
	gtk_widget_show(get_widget(GTK_WIDGET(user_data), "recvWinURLScrollbar"));
}


void
on_sendUserList_click_column           (GtkCList        *clist,
                                        gint             column,
                                        gpointer         user_data)
{
	if (clist->sort_column == column) {
		if (clist->sort_type == GTK_SORT_ASCENDING)
			gtk_clist_set_sort_type(clist, GTK_SORT_DESCENDING);
		else
			gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
	}
	else {
		gtk_clist_set_sort_type(clist, GTK_SORT_ASCENDING);
		gtk_clist_set_sort_column(clist, column);
	}
	gtk_clist_sort(clist);
	option.default_sort_column = column;
	option.default_sort_type = clist->sort_type;
#if 0
	switch (column) {
	case 0:
		db_bsort(db, IPMsgUserListSortName);
		break;
	case 1:
		db_bsort(db, IPMsgUserListSortUser);
		break;
	case 2:
		db_bsort(db, IPMsgUserListSortMachine);
		break;
	case 3:
		db_bsort(db, IPMsgUserListSortGroup);
		break;
	case 4:
		db_bsort(db, IPMsgUserListSortIP);
		break;
	}
	if (user_data) {
		gtk_clist_freeze(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "sendUserList")));
		gtk_clist_clear(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "sendUserList")));
		db_foreach(db, IPMsgSendWinDraw, user_data);
		gtk_clist_thaw(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "sendUserList")));
	}
#endif
}

gboolean
on_sendUserList_button_press_event      (GtkWidget       *widget,
										 GdkEventButton  *event,
										 gpointer         user_data)
{
	switch (event->button) {
	case 2:						/* middle click */
        gtk_menu_popup(GTK_MENU(create_menu_select_group(user_data/*sendUserList*/))
					   , NULL, NULL, NULL, NULL
					   , event->button, event->time);
		break;
	}
	return TRUE;
}


gboolean
on_menu_select_group_cb(GtkObject *object, gpointer user_data)
{
	GtkWidget	*sendUserList;
	gint		i;
	IPMsgList	*data;
	gint		select_row_min = -1;

	sendUserList = get_widget(GTK_WIDGET(menu_select_group_sendUserList), "sendUserList");
	//gtk_clist_unselect_all(GTK_CLIST(sendUserList));
	for (i = 0; i < GTK_CLIST(sendUserList)->rows; i++) {
		data = gtk_clist_get_row_data(GTK_CLIST(sendUserList), i);
		if (strcmp(data->group, user_data) == 0) {
			gtk_clist_select_row(GTK_CLIST(sendUserList), i, 0);
			if (select_row_min == -1)	select_row_min = i;
		}
	}
	if (gtk_clist_row_is_visible (GTK_CLIST(sendUserList), select_row_min) != GTK_VISIBILITY_FULL) {
		gtk_clist_moveto(GTK_CLIST(sendUserList), select_row_min, 0, 0.0, 0.0);
	}
	return TRUE;
}


void
on_buttonWin_destroy                   (GtkObject       *object,
                                        gpointer         user_data)
{
	//IPMsgQuit();
	main_quit();
}


gboolean
on_buttonWin_delete_event              (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{

	return FALSE;
}


gboolean
on_buttonWinEv_key_press_event      (GtkWidget       *widget,
									 GdkEventKey     *event,
									 gpointer         user_data)
{
	//g_print("buttonWin: key press: %d\n", event->keyval);
	switch (event->keyval) {
		/* send */
	case 32:					/* spacebar */
	case 65293:					/* enter */
		if (nIPMsgNonPopup > 0)	IPMsgOpenNonPopup();
		else					IPMsgOpenSendWin();
		break;
	}
	return TRUE;
}


gboolean
on_buttonWinEv_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	GtkWidget	*fuzaiMenu;

	//g_print("buttonWin: button press: %d\n", event->button);
	switch (event->button) {
	case 1:						/* send message */
		if (nIPMsgNonPopup > 0)	IPMsgOpenNonPopup();
		else					IPMsgOpenSendWin();
		break;
	case 2:						/* absence menu */
		if (applet_p()) break;
		fuzaiMenu = create_popupmenu_fuzai();
        gtk_menu_popup(GTK_MENU(fuzaiMenu), NULL, NULL, NULL, NULL,
					   event->button, event->time);
		break;
	default:					/* main menu */
		if (applet_p()) break;
		gnome_popup_menu_do_popup(create_mainmenu(), NULL, NULL, event, NULL);
		break;
	}
	return TRUE;
}


void
on_logWin_destroy                      (GtkObject       *object,
                                        gpointer         user_data)
{

}


gboolean
on_logWin_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{

  return TRUE;
}


void
on_recvWin_destroy                     (GtkObject       *object,
                                        gpointer         user_data)
{
	IPMsgReadMsgData	*rmsg;

	if ((rmsg = (IPMsgReadMsgData*)db_foreach(db_read, IPMsgSearchReadMsg, (void*)object)) != 0) {
		dbd_free(db_read, rmsg->db_tag);
		IPMsgFree_rmsg(rmsg);
	}
}

#ifdef APPLET
gboolean
on_popupmenu_fuzai_applet_cb(AppletWidget *applet, gpointer user_data)
{
	IPMsgFuzaiChange((gint)user_data);
	return TRUE;
}
#endif /* APPLET */

gboolean
on_popupmenu_fuzai_cb(gpointer user_data)
{
	IPMsgFuzaiChange((gint)user_data);
	return TRUE;
}

void on_mainmenu_setting_cb(gpointer user_data)
{
	GtkWidget	*widget;

	widget = gnome_property_box_new();
	gnome_property_box_append_page(GNOME_PROPERTY_BOX(widget), create_prop_main(widget), gtk_label_new(_("Main")));
	gnome_property_box_append_page(GNOME_PROPERTY_BOX(widget), create_prop_absence(widget), gtk_label_new(_("Absence")));
	gnome_property_box_append_page(GNOME_PROPERTY_BOX(widget), create_prop_broadcast(widget), gtk_label_new(_("Broadcast")));
	gnome_property_box_append_page(GNOME_PROPERTY_BOX(widget), create_prop_sound(widget), gtk_label_new(_("Sound")));
	gtk_widget_show_all(widget);
	gtk_signal_connect (GTK_OBJECT (widget), "apply",
						GTK_SIGNAL_FUNC (on_prop_apply),
						widget);
}

void on_mainmenu_show_log_cb(gpointer user_data)
{
	GtkWidget *gl;
	GtkWidget *dialog;
	//GdkFont	*font;

	gl = gnome_less_new();
	//font = gdk_font_load("-adobe-helvetica-bold-o-normal-*-*-140-*-*-p-*-iso8859-1");
	//gnome_less_set_font(GNOME_LESS(gl), font);
	//gnome_less_show_string(GNOME_LESS(gl), "abcdefghijkl");
#if 1
	if (gnome_less_show_file(GNOME_LESS(gl), option.log_fname) == FALSE) {
		dialog = gnome_message_box_new (
			GNOME_MESSAGE_BOX_ERROR,
			_("Can not view file."),
			_("OK"),
			NULL);
		gnome_dialog_run (GNOME_DIALOG (dialog));
	}
#endif
}

#if 0
void on_mainmenu_absence_cb(gpointer user_data)
{
	GtkWidget	*fuzaiMenu;

	fuzaiMenu = create_popupmenu_fuzai();
#ifdef APPLET
	if (applet_p())
		gtk_menu_popup(GTK_MENU(fuzaiMenu), NULL, NULL, NULL, NULL,
					   0, 0);
	else
#endif /* APPLET */
		gtk_menu_popup(GTK_MENU(fuzaiMenu), NULL, NULL, NULL, NULL,
					   event->button, event->time);
}
#endif

void on_mainmenu_about_cb(gpointer user_data)
{
	const gchar *authors[] = {
		_("H.Shirouzu"),
		_("T.Matsumaru"),
		NULL
	};

	gtk_widget_show(gnome_about_new("GIPMSG", VERSION,
									"Copyright(c)1996-1998,1999",
									(const gchar **)authors,
									_("IP Messenger for X Window"
									  "\n\nUDP based communication application over LAN"),
									NULL));
}

void
on_sendDohoTB_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	gint		i;
	GtkCList	*clist;
	GdkColor	color = { 0, 50000, 50000, 50000 };

	clist = GTK_CLIST(user_data);
	if (togglebutton->active) {
		for (i = 0; i < clist->rows; i++) {
			gtk_clist_set_foreground(clist, i, &color);
			gtk_clist_set_selectable(clist, i, 0);
		}
	}
	else {
		for (i = 0; i < clist->rows; i++) {
			gtk_clist_set_foreground(clist, i, NULL);
			gtk_clist_set_selectable(clist, i, 1);
		}
	}
}

void on_retryWin_retry_clicked(GtkButton       *button,
							   gpointer			user_data)
{
	IPMsgSentMsg	*smsg = user_data;

	gtk_widget_destroy(GTK_WIDGET(get_widget_window(GTK_WIDGET(button))));
	smsg->timeout_tag = gtk_timeout_add(IPMSG_TIMERINTERVAL, IPMsgReSendData, smsg);
	smsg->retry = 0;
}

void on_retryWin_cancel_clicked(GtkButton       *button,
								gpointer		user_data)
{
	IPMsgSentMsg	*smsg = user_data;

	gtk_widget_destroy(GTK_WIDGET(get_widget_window(GTK_WIDGET(button))));
	dbd_free(db_send, smsg->db_tag);
	IPMsgFree_smsg(smsg);
}
