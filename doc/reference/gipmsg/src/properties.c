#include <stdio.h>
#include <string.h>
#include "../config.h"
#include <gnome.h>
#include <applet-widget.h>
#include <arpa/inet.h>

#define PROPERTIES_C__

#include "main.h"
#include "properties.h"
#include "win.h"
#include "sig.h"
#include "ipmsg.h"


struct OPTION_T option = {
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	NULL,
};


void load_properties(char *path, struct OPTION_T *prop )
{
	gboolean def;
	static struct {
		gchar	*label;
		gchar	*msg;
	} absence_msg[] = {
		{ N_("Away"),		N_("I'm away, now.") },
		{ N_("Lunch"),		N_("I am getting lunch.") },
		{ N_("Conference"),	N_("conference") },
		{ N_("Visitor"),	N_("I've got visitor.") },
		{ N_("Gone"),		N_("i've get out now.") },
		{ N_("Homecoming"),	N_("homecoming") },
		{ N_("ramble"),		N_("Rambling Rambling Rambling") },
		{ N_("hehe"),		N_("BowWow!!") },
	};
	gint		i;
	gchar		buf[4096];

	//g_print("load_properties: %s\n", path);
	gnome_config_push_prefix(path);
	/*--- main ---*/
	sprintf(buf, "main/nick=%s", option.user);
	prop->nick = gnome_config_get_string(buf);
	prop->group = gnome_config_get_string("main/group=");
	prop->check_seal = gnome_config_get_int_with_default("main/check_seal=1", &def);
	prop->non_popup = gnome_config_get_int_with_default("main/non_popup=0", &def);
	prop->seal = gnome_config_get_int_with_default("main/default_seal_check=0", &def);
	prop->cite = gnome_config_get_int_with_default("main/default_cite_check=0", &def);
	prop->log = gnome_config_get_int_with_default("main/log=0", &def);
	prop->log_fname = gnome_config_get_string("main/log_fname=ipmsg.log");
	prop->beep = gnome_config_get_int_with_default("sound/beep=0", &def);
	prop->snd = gnome_config_get_int_with_default("sound/snd=0", &def);
	prop->snd_fname = gnome_config_get_string("sound/snd_fname=");
	/*--- absence ---*/
	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		sprintf(buf, "absence/%u=%s", i, absence_msg[i].label);
		prop->absence[i] = gnome_config_get_string(buf);
		sprintf(buf, "absence/%u_=%s", i, absence_msg[i].msg);
		prop->absence_text[i] = gnome_config_get_string(buf);
	}
	/*--- broadcast address ---*/
	prop->broadcasts = gnome_config_get_string("broadcast/addresses=");
	/*--- shortcut_user ---*/
	for (i = 0; i < SHORTCUT_USER_NUM; i++) {
		sprintf(buf, "shortcut_user/%u=", i);
		prop->shortcut_user[i] = gnome_config_get_string(buf);
	}
	gnome_config_pop_prefix();
	//g_print("load_properties: done\n");
}


void save_properties(char *path, struct OPTION_T *prop )
{
	gint		i;
	gchar		buf[4096];

	//g_print("save_properties: %s\n", path);
	gnome_config_push_prefix(path);
	/*--- main ---*/
	gnome_config_set_string("main/nick", prop->nick);
	gnome_config_set_string("main/group", prop->group);
	gnome_config_set_int("main/check_seal", prop->check_seal);
	gnome_config_set_int("main/non_popup", prop->non_popup);
	gnome_config_set_int("main/default_seal_check", prop->seal);
	gnome_config_set_int("main/default_cite_check", prop->cite);
	gnome_config_set_int("main/log", prop->log);
	gnome_config_set_string("main/log_fname", prop->log_fname);
	gnome_config_set_int("sound/beep", prop->beep);
	gnome_config_set_int("sound/snd", prop->snd);
	gnome_config_set_string("sound/snd_fname", prop->snd_fname);
	/*--- absence ---*/
	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		sprintf(buf, "absence/%u", i);
		gnome_config_set_string(buf, prop->absence[i]);
		sprintf(buf, "absence/%u_", i);
		gnome_config_set_string(buf, prop->absence_text[i]);
	}
	/*--- broadcast address ---*/
	gnome_config_set_string("broadcast/addresses", prop->broadcasts);
	/*--- shortcut_user ---*/
	for (i = 0; i < SHORTCUT_USER_NUM; i++) {
		sprintf(buf, "shortcut_user/%u", i);
		gnome_config_set_string(buf, prop->shortcut_user[i]);
	}
	gnome_config_sync();
    gnome_config_drop_all();	/* ? */
	gnome_config_pop_prefix();
	//g_print("save_properties: done\n");
}


/*================================================================
 *	signals
 *================================================================*/
void on_prop_changed(GtkEditable *widget, gpointer user_data)
{
	if (GTK_NOTEBOOK (GNOME_PROPERTY_BOX(user_data)->notebook)->cur_page != 0) {
		//g_print("prop: changed\n");
		gnome_property_box_changed(GNOME_PROPERTY_BOX(user_data));
	}
}


void on_prop_apply(GnomePropertyBox *propertybox,
				   gint arg1,
				   gpointer user_data)
{
	gint		i, n;
	gchar		*p;
	gchar		**parray, **q;

	//if (arg1 != -1)	return;
	//g_print("prop: apply:%d,$%p\n", arg1, user_data);
	switch (arg1) {
	case 0:
		/* main */
		g_free(option.nick);
		option.nick = g_strdup(gtk_entry_get_text(GTK_ENTRY(get_widget(GTK_WIDGET(user_data), "prop1_nick"))));
		g_free(option.group);
		option.group = g_strdup(gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(get_widget(GTK_WIDGET(user_data), "prop1_group"))->entry)));
		option.check_seal = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop1_opt_check_seal")));
		option.non_popup = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop1_opt_non_popup")));
		option.seal = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop1_opt_seal")));
		option.cite = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop1_opt_cite")));
		option.log = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop1_log_log")));
		g_free(option.log_fname);
		option.log_fname = g_strdup(gtk_entry_get_text(
			GTK_ENTRY(gnome_file_entry_gtk_entry(
				GNOME_FILE_ENTRY(get_widget(GTK_WIDGET(user_data), "prop1_log_entry"))
				))
			));
		break;
	case 1:
		/* absence */
		for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
			g_free(option.absence[i]);
			g_free(option.absence_text[i]);
			gtk_clist_get_text(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "prop3_absence_clist")), i, 1, &p);
			option.absence[i] = g_strdup(p);
			gtk_clist_get_text(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "prop3_absence_clist")), i, 2, &p);
			option.absence_text[i] = g_strdup(p);
		}
#ifdef APPLET
		if (applet_p()) {
			AppletUnregistAbsence(winMain);
			AppletRegistAbsence(winMain);
		}
#endif
		break;
	case 2:
		/* broadcasts */
		n = GTK_CLIST(get_widget(GTK_WIDGET(user_data), "prop2_bcast_clist"))->rows;
		q = parray = g_malloc0(sizeof(gchar*) * (n+1));
		for (i = 0; i < n; i++, q++) {
			gtk_clist_get_text(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "prop2_bcast_clist"))
							   , i, 0, q);
		}
		*q = 0;
		g_free(option.broadcasts);
		option.broadcasts = g_strjoinv(",", parray);
		IPMsgSetBcasts(option.localnet_bcast, option.broadcasts);
		g_free(parray);
		break;
	case 3:
		/* sound */
		option.beep = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop4_snd_beep")));
		option.snd = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(user_data), "prop4_snd_wav")));
		g_free(option.snd_fname);
		option.snd_fname = g_strdup(gtk_entry_get_text(
			GTK_ENTRY(gnome_file_entry_gtk_entry(
				GNOME_FILE_ENTRY(get_widget(GTK_WIDGET(user_data), "prop4_snd_wav_fname"))
				))
			));
		break;
	case -1:
		/* save properties */
		save_properties("gipmsg/", &option);
		break;
	}
}


/*
 * main tag
 */


/*
 * broadcast tag
 */
void
on_prop2_bcast_bt_add(GtkButton *button, gpointer user_data)
{
	struct in_addr addr;
	gchar	*ip;
	gchar	*item[2];

	ip = gtk_entry_get_text(GTK_ENTRY(get_widget(GTK_WIDGET(user_data), "prop2_bcast_entry")));
	if (inet_aton(ip, &addr) != 0) {	/* okay */
		gtk_editable_delete_text(GTK_EDITABLE(get_widget(GTK_WIDGET(user_data), "prop2_bcast_entry")), 0, -1);
		item[0] = ip;
		item[1] = 0;
		gtk_clist_append(GTK_CLIST(get_widget(GTK_WIDGET(user_data), "prop2_bcast_clist")), item);

		on_prop_changed(user_data, user_data);
	}
}


void
on_prop2_bcast_bt_remove(GtkButton *button, gpointer user_data)
{
	GtkCList	*clist;
	GList		*p;

	//g_print("remove\n");
	clist = (GtkCList*)get_widget(GTK_WIDGET(user_data), "prop2_bcast_clist");
	for (p = clist->selection; p; p = p->next) {
		//g_print(" : %d\n", (gint)p->data);
		gtk_clist_remove(clist, (gint)p->data);
		if (p == clist->selection_end)	break;
		/*
		 * I can't understand why I remove some articles.
		 * only one article is okay.
		 * this is gtk+ bug?
		 */
		break;
	}

	on_prop_changed(user_data, user_data);
}


/*
 * absence tag
 */
void
on_prop3_absence_select_row(GtkCList *clist,
							gint row,
							gint column,
							GdkEventButton *event,
							gpointer user_data)
{
	GtkWidget	*widget;
	gchar		*p;

	widget = user_data;
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_bt")), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_entry")), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_text")), TRUE);
	gtk_clist_get_text(clist, row, 1, &p);
	gtk_entry_set_text(GTK_ENTRY(get_widget(widget, "prop3_absence_entry")), p);
	gtk_clist_get_text(clist, row, 2, &p);
	gtk_editable_delete_text(GTK_EDITABLE(get_widget(widget, "prop3_absence_text")), 0, -1);
	gtk_text_insert(GTK_TEXT(get_widget(widget, "prop3_absence_text")), NULL, NULL, NULL, p, -1);
}


void
on_prop3_absence_unselect_row(GtkCList        *clist,
							  gint             row,
							  gint             column,
							  GdkEvent        *event,
							  gpointer         user_data)
{
	GtkWidget	*widget;

	widget = user_data;
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_bt")), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_entry")), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(widget), "prop3_absence_text")), FALSE);
	gtk_entry_set_text(GTK_ENTRY(get_widget(widget, "prop3_absence_entry")), "");
	gtk_editable_delete_text(GTK_EDITABLE(get_widget(widget, "prop3_absence_text")), 0, -1);
}


void
on_prop3_absence_bt(GtkButton *button, gpointer user_data)
{
	GList		*p;
	GtkCList	*clist;
	gchar		*title;
	gchar		*str;

	title = gtk_entry_get_text(GTK_ENTRY(get_widget(GTK_WIDGET(user_data), "prop3_absence_entry")));
	str = gtk_editable_get_chars(GTK_EDITABLE(get_widget(GTK_WIDGET(user_data), "prop3_absence_text")), 0, -1);
	clist = (GtkCList*)get_widget(GTK_WIDGET(user_data), "prop3_absence_clist");
	for (p = clist->selection; p; p = p->next) {
		gtk_clist_set_text(clist, (gint)p->data, 1, title);
		gtk_clist_set_text(clist, (gint)p->data, 2, str);
		if (p == clist->selection_end)	break;
	}
	on_prop_changed(user_data, user_data);
}


/*
 * sound tag
 */
void on_prop_snd_play(GtkEditable *widget, gpointer user_data)
{
	gnome_sound_play(gtk_entry_get_text(
		GTK_ENTRY(gnome_file_entry_gtk_entry(
			GNOME_FILE_ENTRY(get_widget(GTK_WIDGET(user_data), "prop4_snd_wav_fname"))
			))
		));
}


/*================================================================
 *	windows
 *================================================================*/
GtkWidget*
create_prop_main (GtkWidget *widget)
{
	GtkWidget *window4;
	GtkWidget *vbox1;
	GtkWidget *hbox1;
	GtkWidget *frame1;
	GtkWidget *prop1_nick;
	GtkWidget *frame2;
	GtkWidget *prop1_group;
	GList *prop1_group_items = NULL;
	GtkWidget *frame5;
	GtkWidget *vbox3;
	GtkWidget *prop1_opt_check_seal;
	GtkWidget *prop1_opt_non_popup;
	GtkWidget *prop1_opt_seal;
	GtkWidget *prop1_opt_cite;
	GtkWidget *frame8;
	GtkWidget *vbox6;
	GtkWidget *prop1_log_log;
	GtkWidget *prop1_log_entry;

	window4 = vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox1);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, FALSE, 0);

	frame1 = gtk_frame_new (_("Nickname"));
	gtk_widget_show (frame1);
	gtk_box_pack_start (GTK_BOX (hbox1), frame1, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (frame1), 4);
	gtk_frame_set_label_align (GTK_FRAME (frame1), 0.05, 0.5);

	prop1_nick = gtk_entry_new ();
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_nick", prop1_nick);
	gtk_widget_show (prop1_nick);
	gtk_container_add (GTK_CONTAINER (frame1), prop1_nick);
	gtk_entry_set_editable(GTK_ENTRY(prop1_nick), TRUE);
	gtk_entry_set_text(GTK_ENTRY(prop1_nick), option.nick);
	//gtk_entry_set_max_length(GTK_ENTRY(prop1_nick), 30);
	gtk_signal_connect (GTK_OBJECT (prop1_nick), "changed",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	frame2 = gtk_frame_new (_("Group"));
	gtk_widget_show (frame2);
	gtk_box_pack_start (GTK_BOX (hbox1), frame2, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (frame2), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame2), 0.05, 0.5);

	prop1_group = gtk_combo_new ();
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_group", prop1_group);
	gtk_widget_show (prop1_group);
	gtk_container_add (GTK_CONTAINER (frame2), prop1_group);
	gtk_container_border_width (GTK_CONTAINER (prop1_group), 10);
	//prop1_group_items = g_list_append (prop1_group_items, option.group);
	prop1_group_items = IPMsgGetGroupList();
	prop1_group_items = g_list_prepend (prop1_group_items, option.group);
	gtk_combo_set_popdown_strings (GTK_COMBO (prop1_group), prop1_group_items);
	g_list_free (prop1_group_items);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO(prop1_group)->entry), "changed",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	frame5 = gtk_frame_new (_("Option"));
	gtk_widget_show (frame5);
	gtk_box_pack_start (GTK_BOX (vbox1), frame5, FALSE, FALSE, 0);
	gtk_container_border_width (GTK_CONTAINER (frame5), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame5), 0.05, 0.5);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (frame5), vbox3);

	prop1_opt_check_seal = gtk_check_button_new_with_label (_("check opened seal"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_opt_check_seal", prop1_opt_check_seal);
	gtk_widget_show (prop1_opt_check_seal);
	gtk_box_pack_start (GTK_BOX (vbox3), prop1_opt_check_seal, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop1_opt_check_seal), option.check_seal);
	gtk_signal_connect (GTK_OBJECT (prop1_opt_check_seal), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop1_opt_non_popup = gtk_check_button_new_with_label (_("non-popup message"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_opt_non_popup", prop1_opt_non_popup);
	gtk_widget_show (prop1_opt_non_popup);
	gtk_box_pack_start (GTK_BOX (vbox3), prop1_opt_non_popup, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop1_opt_non_popup), option.non_popup);
	gtk_signal_connect (GTK_OBJECT (prop1_opt_non_popup), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop1_opt_seal = gtk_check_button_new_with_label (_("default use seal"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_opt_seal", prop1_opt_seal);
	gtk_widget_show (prop1_opt_seal);
	gtk_box_pack_start (GTK_BOX (vbox3), prop1_opt_seal, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop1_opt_seal), option.seal);
	gtk_signal_connect (GTK_OBJECT (prop1_opt_seal), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop1_opt_cite = gtk_check_button_new_with_label (_("default use cite"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_opt_cite", prop1_opt_cite);
	gtk_widget_show (prop1_opt_cite);
	gtk_box_pack_start (GTK_BOX (vbox3), prop1_opt_cite, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop1_opt_cite), option.cite);
	gtk_signal_connect (GTK_OBJECT (prop1_opt_cite), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	frame8 = gtk_frame_new (_("Log"));
	gtk_widget_show (frame8);
	gtk_box_pack_start (GTK_BOX (vbox1), frame8, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (frame8), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame8), 0.05, 0.5);

	vbox6 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox6);
	gtk_container_add (GTK_CONTAINER (frame8), vbox6);

	prop1_log_log = gtk_check_button_new_with_label (_("logging"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_log_log", prop1_log_log);
	gtk_widget_show (prop1_log_log);
	gtk_box_pack_start (GTK_BOX (vbox6), prop1_log_log, TRUE, TRUE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop1_log_log), option.log);
	gtk_signal_connect (GTK_OBJECT (prop1_log_log), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

    prop1_log_entry = gnome_file_entry_new(option.log_fname, _("Select Log file"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop1_log_entry", prop1_log_entry);
	gtk_widget_show (prop1_log_entry);
	gtk_box_pack_start (GTK_BOX (vbox6), prop1_log_entry, TRUE, TRUE, 0);
	gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(prop1_log_entry))), option.log_fname);
	gtk_signal_connect (GTK_OBJECT (gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(prop1_log_entry))), "changed",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	return window4;
}

GtkWidget*
create_prop_broadcast (GtkWidget *widget)
{
	GtkWidget *window5;
	GtkWidget *frame7;
	GtkWidget *hbox4;
	GtkWidget *vbox4;
	GtkWidget *vbox5;
	GtkWidget *dummy1;
	GtkWidget *label4;
	GtkWidget *prop2_bcast_entry;
	GtkWidget *hbox5;
	GtkWidget *prop2_bcast_bt_add;
	GtkWidget *prop2_bcast_bt_remove;
	GtkWidget *scrolledwindow4;
	GtkWidget *prop2_bcast_clist;
	GtkWidget *label5;
	struct DB_DAT_T	*dbd;
	gchar	*item[2];

	window5 = frame7 = gtk_frame_new (_("Broadcast for out of Local Network"));
	gtk_widget_show (frame7);
	gtk_container_border_width (GTK_CONTAINER (frame7), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame7), 0.05, 0.5);

	hbox4 = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (widget), "hbox4", hbox4);
	gtk_widget_show (hbox4);
	gtk_container_add (GTK_CONTAINER (frame7), hbox4);

	vbox4 = gtk_vbox_new (FALSE, 192);
	gtk_object_set_data (GTK_OBJECT (widget), "vbox4", vbox4);
	gtk_widget_show (vbox4);
	gtk_box_pack_start (GTK_BOX (hbox4), vbox4, FALSE, FALSE, 0);

	vbox5 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (widget), "vbox5", vbox5);
	gtk_widget_show (vbox5);
	gtk_box_pack_start (GTK_BOX (vbox4), vbox5, TRUE, TRUE, 0);

	dummy1 = gtk_label_new ("");
	gtk_object_set_data (GTK_OBJECT (widget), "dummy1", dummy1);
	gtk_widget_show (dummy1);
	gtk_box_pack_start (GTK_BOX (vbox5), dummy1, TRUE, TRUE, 0);

	label4 = gtk_label_new (_("(ex. 192.168.128.255)"));
	gtk_object_set_data (GTK_OBJECT (widget), "label4", label4);
	gtk_widget_show (label4);
	gtk_box_pack_start (GTK_BOX (vbox5), label4, FALSE, FALSE, 0);

	prop2_bcast_entry = gtk_entry_new_with_max_length (15);
	gtk_object_set_data (GTK_OBJECT (widget), "prop2_bcast_entry", prop2_bcast_entry);
	gtk_widget_show (prop2_bcast_entry);
	gtk_box_pack_start (GTK_BOX (vbox5), prop2_bcast_entry, FALSE, FALSE, 0);

	hbox5 = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (widget), "hbox5", hbox5);
	gtk_widget_show (hbox5);
	gtk_box_pack_start (GTK_BOX (vbox5), hbox5, FALSE, FALSE, 0);

	prop2_bcast_bt_add = gtk_button_new_with_label (_("add"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop2_bcast_bt_add", prop2_bcast_bt_add);
	gtk_widget_show (prop2_bcast_bt_add);
	gtk_box_pack_start (GTK_BOX (hbox5), prop2_bcast_bt_add, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (prop2_bcast_bt_add), 10);
	gtk_signal_connect (GTK_OBJECT (prop2_bcast_bt_add), "clicked",
						GTK_SIGNAL_FUNC (on_prop2_bcast_bt_add),
						widget);

	prop2_bcast_bt_remove = gtk_button_new_with_label (_("remove"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop2_bcast_bt_remove", prop2_bcast_bt_remove);
	gtk_widget_show (prop2_bcast_bt_remove);
	gtk_box_pack_start (GTK_BOX (hbox5), prop2_bcast_bt_remove, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (prop2_bcast_bt_remove), 10);
	gtk_signal_connect (GTK_OBJECT (prop2_bcast_bt_remove), "clicked",
						GTK_SIGNAL_FUNC (on_prop2_bcast_bt_remove),
						widget);

	scrolledwindow4 = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (widget), "scrolledwindow4", scrolledwindow4);
	gtk_widget_show (scrolledwindow4);
	gtk_box_pack_start (GTK_BOX (hbox4), scrolledwindow4, TRUE, TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (scrolledwindow4), 5);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow4), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	prop2_bcast_clist = gtk_clist_new (1);
	gtk_object_set_data (GTK_OBJECT (widget), "prop2_bcast_clist", prop2_bcast_clist);
	gtk_widget_show (prop2_bcast_clist);
	gtk_container_add (GTK_CONTAINER (scrolledwindow4), prop2_bcast_clist);
	gtk_clist_set_column_width (GTK_CLIST (prop2_bcast_clist), 0, 80);
	//gtk_clist_set_selection_mode (GTK_CLIST (prop2_bcast_clist), GTK_SELECTION_EXTENDED);
	gtk_clist_column_titles_hide (GTK_CLIST (prop2_bcast_clist));

	gtk_clist_freeze(GTK_CLIST (prop2_bcast_clist));
	for (dbd = db_broadcast->top; dbd != 0; dbd = dbd->next) {
		if (db_broadcast->top == dbd) continue;	/* skip */
		item[0] = (char*)inet_ntoa(*(struct in_addr*)dbd->data);
		item[1] = 0;
		gtk_clist_append (GTK_CLIST (prop2_bcast_clist), item);
	}	
	gtk_clist_thaw(GTK_CLIST (prop2_bcast_clist));

	label5 = gtk_label_new ("label5");
	gtk_object_set_data (GTK_OBJECT (widget), "label5", label5);
	gtk_widget_show (label5);
	gtk_clist_set_column_widget (GTK_CLIST (prop2_bcast_clist), 0, label5);

	return window5;
}


GtkWidget*
create_prop_absence (GtkWidget *widget)
{
	GtkWidget *window6;
	GtkWidget *frame9;
	GtkWidget *vpaned1;
	GtkWidget *scrolledwindow5;
	GtkWidget *prop3_absence_clist;
	GtkWidget *label6;
	GtkWidget *label7;
	GtkWidget *label8;
	GtkWidget *vbox7;
	GtkWidget *hbox9;
	GtkWidget *label9;
	GtkWidget *prop3_absence_bt;
	GtkWidget *prop3_absence_entry;
	GtkWidget *scrolledwindow6;
	GtkWidget *prop3_absence_text;
	gchar	*item[4];
	gchar	buf[4];
	gint		i;

	window6 = frame9 = gtk_frame_new (_("Absence Messages"));
	gtk_widget_show (frame9);
	//gtk_container_add (GTK_CONTAINER (window6), frame9);
	gtk_container_border_width (GTK_CONTAINER (frame9), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame9), 0.05, 0.5);

	vpaned1 = gtk_vpaned_new();
	gtk_paned_set_gutter_size(GTK_PANED (vpaned1), 8);
	//gtk_paned_set_position(GTK_PANED (vpaned1), 320);
	gtk_widget_show(vpaned1);
	gtk_container_add (GTK_CONTAINER (frame9), vpaned1);
	
	scrolledwindow5 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow5);
	gtk_paned_pack1 (GTK_PANED (vpaned1), scrolledwindow5, TRUE, TRUE);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow5), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	prop3_absence_clist = gtk_clist_new (3);
	gtk_object_set_data (GTK_OBJECT (widget), "prop3_absence_clist", prop3_absence_clist);
	gtk_widget_show (prop3_absence_clist);
	gtk_container_add (GTK_CONTAINER (scrolledwindow5), prop3_absence_clist);
	gtk_clist_set_column_auto_resize(GTK_CLIST (prop3_absence_clist), 0, 1);
	gtk_clist_set_column_auto_resize(GTK_CLIST (prop3_absence_clist), 1, 1);
	gtk_clist_set_column_auto_resize(GTK_CLIST (prop3_absence_clist), 2, 1);
	//gtk_clist_set_column_width (GTK_CLIST (prop3_absence_clist), 0, 20);
	//gtk_clist_set_column_width (GTK_CLIST (prop3_absence_clist), 1, 160);
	//gtk_clist_set_column_width (GTK_CLIST (prop3_absence_clist), 2, 500);
	gtk_clist_column_titles_show (GTK_CLIST (prop3_absence_clist));
	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		sprintf(buf, "%u", i+1);
		item[0] = buf;
		item[1] = option.absence[i];
		item[2] = option.absence_text[i];
		item[3] = 0;
		gtk_clist_set_row_data(GTK_CLIST(prop3_absence_clist), gtk_clist_append(GTK_CLIST(prop3_absence_clist), item), NULL);
	}
	gtk_signal_connect (GTK_OBJECT (prop3_absence_clist), "select-row",
						GTK_SIGNAL_FUNC (on_prop3_absence_select_row),
						widget);
	gtk_signal_connect (GTK_OBJECT (prop3_absence_clist), "unselect-row",
						GTK_SIGNAL_FUNC (on_prop3_absence_unselect_row),
						widget);
	
	label6 = gtk_label_new (_("No"));
	gtk_widget_show (label6);
	gtk_clist_set_column_widget (GTK_CLIST (prop3_absence_clist), 0, label6);

	label7 = gtk_label_new (_("Title"));
	gtk_widget_show (label7);
	gtk_clist_set_column_widget (GTK_CLIST (prop3_absence_clist), 1, label7);

	label8 = gtk_label_new (_("Message"));
	gtk_widget_show (label8);
	gtk_clist_set_column_widget (GTK_CLIST (prop3_absence_clist), 2, label8);

	vbox7 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox7);
	gtk_paned_pack2 (GTK_PANED (vpaned1), vbox7, TRUE, TRUE);

	hbox9 = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox9);
	gtk_box_pack_start (GTK_BOX (vbox7), hbox9, FALSE, TRUE, 0);

	prop3_absence_bt = gtk_button_new_with_label(_("Change"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop3_absence_bt", prop3_absence_bt);
	gtk_widget_show (prop3_absence_bt);
	gtk_box_pack_start (GTK_BOX (hbox9), prop3_absence_bt, FALSE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (prop3_absence_bt), "clicked",
						GTK_SIGNAL_FUNC (on_prop3_absence_bt),
						widget);
	gtk_widget_set_sensitive(GTK_WIDGET(prop3_absence_bt), FALSE);

	label9 = gtk_label_new (_("Title"));
	gtk_widget_show (label9);
	gtk_box_pack_start (GTK_BOX (hbox9), label9, TRUE, TRUE, 0);
	gtk_label_set_justify (GTK_LABEL (label9), GTK_JUSTIFY_RIGHT);
	gtk_misc_set_padding (GTK_MISC (label9), 10, 10);

	prop3_absence_entry = gtk_entry_new ();
	gtk_object_set_data (GTK_OBJECT (widget), "prop3_absence_entry", prop3_absence_entry);
	gtk_widget_show (prop3_absence_entry);
	gtk_entry_set_editable(GTK_ENTRY(prop3_absence_entry), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox9), prop3_absence_entry, TRUE, TRUE, 0);
	gtk_widget_set_sensitive(GTK_WIDGET(prop3_absence_entry), FALSE);

	scrolledwindow6 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow6);
	gtk_box_pack_start (GTK_BOX (vbox7), scrolledwindow6, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow6), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	prop3_absence_text = gtk_text_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (widget), "prop3_absence_text", prop3_absence_text);
	gtk_widget_show (prop3_absence_text);
	gtk_container_add (GTK_CONTAINER (scrolledwindow6), prop3_absence_text);
	gtk_text_set_editable (GTK_TEXT (prop3_absence_text), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(prop3_absence_text), FALSE);

	return window6;
}


GtkWidget*
create_prop_sound (GtkWidget *widget)
{
	GtkWidget *window7;
	GtkWidget *frame10;
	GtkWidget *vbox8;
	GtkWidget *prop4_snd_beep;
	GtkWidget *prop4_snd_wav;
	GtkWidget *prop4_snd_wav_fname;
	GtkWidget *prop4_snd_bt;

	window7 = frame10 = gtk_frame_new (_("Sound"));
	gtk_object_set_data (GTK_OBJECT (widget), "frame10", frame10);
	gtk_widget_show (frame10);
	//gtk_container_add (GTK_CONTAINER (window7), frame10);
	gtk_container_border_width (GTK_CONTAINER (frame10), 5);
	gtk_frame_set_label_align (GTK_FRAME (frame10), 0.05, 0.5);

	vbox8 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (widget), "vbox8", vbox8);
	gtk_widget_show (vbox8);
	gtk_container_add (GTK_CONTAINER (frame10), vbox8);

	prop4_snd_beep = gtk_check_button_new_with_label (_("beep"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop4_snd_beep", prop4_snd_beep);
	gtk_widget_show (prop4_snd_beep);
	gtk_box_pack_start (GTK_BOX (vbox8), prop4_snd_beep, FALSE, FALSE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop4_snd_beep), option.beep);
	gtk_signal_connect (GTK_OBJECT (prop4_snd_beep), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop4_snd_wav = gtk_check_button_new_with_label (_("Play Sound"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop4_snd_wav", prop4_snd_wav);
	gtk_widget_show (prop4_snd_wav);
	gtk_box_pack_start (GTK_BOX (vbox8), prop4_snd_wav, FALSE, FALSE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prop4_snd_wav), option.snd);
	gtk_signal_connect (GTK_OBJECT (prop4_snd_wav), "clicked",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop4_snd_wav_fname = gnome_file_entry_new(option.snd_fname, _("Select Sound file"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop4_snd_wav_fname", prop4_snd_wav_fname);
	gtk_widget_show (prop4_snd_wav_fname);
	gtk_box_pack_start (GTK_BOX (vbox8), prop4_snd_wav_fname, FALSE, FALSE, 0);
	gtk_entry_set_text(GTK_ENTRY(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(prop4_snd_wav_fname))), option.snd_fname);
	gtk_signal_connect (GTK_OBJECT (gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(prop4_snd_wav_fname))), "changed",
						GTK_SIGNAL_FUNC (on_prop_changed),
						widget);

	prop4_snd_bt = gtk_button_new_with_label (_("Play"));
	gtk_object_set_data (GTK_OBJECT (widget), "prop4_snd_bt", prop4_snd_bt);
	gtk_widget_show (prop4_snd_bt);
	gtk_box_pack_start (GTK_BOX (vbox8), prop4_snd_bt, FALSE, FALSE, 0);
	gtk_container_border_width (GTK_CONTAINER (prop4_snd_bt), 10);
	gtk_signal_connect (GTK_OBJECT (prop4_snd_bt), "clicked",
						GTK_SIGNAL_FUNC (on_prop_snd_play),
						widget);

	return window7;
}

