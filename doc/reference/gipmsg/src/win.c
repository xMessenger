/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  gipmsg
 *  Copyright (C) <YEAR> <AUTHORS>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#include <gnome.h>
#include <gdk/gdkkeysyms.h>
#ifdef APPLET
#  include <applet-widget.h>
#endif
#include "properties.h"
#include "sig.h"
#include "win.h"
#include "ipmsg.h"
#include "kanji.h"
#include "main.h"

GtkWidget*
get_widget                             (GtkWidget       *widget,
                                        gchar           *widget_name)
{
	GtkWidget *parent, *found_widget;

	for (;;)
    {
		if (GTK_IS_MENU (widget))
			parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
		else
			parent = widget->parent;
		if (parent == NULL)
			break;
		widget = parent;
    }

	found_widget = (GtkWidget*) gtk_object_get_data (GTK_OBJECT (widget),
													 widget_name);
	if (!found_widget)
		g_warning ("Widget not found: %s", widget_name);
	return found_widget;
}

GtkWidget* get_widget_window(GtkWidget *widget)
{
	GtkWidget *parent;

	for (;;)
    {
		if (GTK_IS_MENU (widget))
			parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
		else
			parent = widget->parent;
		if (parent == NULL)
			break;
		widget = parent;
    }
	return widget;
}

/* This is an internally used function to set notebook tab widgets. */
void
set_notebook_tab                       (GtkWidget       *notebook,
                                        gint             page_num,
                                        GtkWidget       *widget)
{
	GtkNotebookPage *page;
	GtkWidget *notebook_page;

	page = (GtkNotebookPage*) g_list_nth (GTK_NOTEBOOK (notebook)->children, page_num)->data;
	notebook_page = page->child;
	gtk_widget_ref (notebook_page);
	gtk_notebook_remove_page (GTK_NOTEBOOK (notebook), page_num);
	gtk_notebook_insert_page (GTK_NOTEBOOK (notebook), notebook_page,
							  widget, page_num);
	gtk_widget_unref (notebook_page);
}

static GList *pixmaps_directories = NULL;

/* Use this function to set the directory containing installed pixmaps. */
void
add_pixmap_directory                   (gchar           *directory)
{
	pixmaps_directories = g_list_prepend (pixmaps_directories, g_strdup (directory));
}

/* This is an internally used function to check if a pixmap file exists. */
#ifndef G_DIR_SEPARATOR_S
#define G_DIR_SEPARATOR_S "/"
#endif
gchar*
check_file_exists                      (gchar           *directory,
                                        gchar           *filename)
{
	gchar *full_filename;
	struct stat s;
	gint status;

	full_filename = g_malloc (strlen (directory) + 1 + strlen (filename) + 1);
	strcpy (full_filename, directory);
	strcat (full_filename, G_DIR_SEPARATOR_S);
	strcat (full_filename, filename);

	status = stat (full_filename, &s);
	if (status == 0 && S_ISREG (s.st_mode))
		return full_filename;
	g_free (full_filename);
	return NULL;
}

/* This is an internally used function to create pixmaps. */
GtkWidget*
create_pixmap                          (GtkWidget       *widget,
                                        gchar           *filename)
{
	gchar *found_filename = NULL;
	GdkColormap *colormap;
	GdkPixmap *gdkpixmap;
	GdkBitmap *mask;
	GtkWidget *pixmap;
	GList *elem;

	/* We first try any pixmaps directories set by the application. */
	elem = pixmaps_directories;
	while (elem)
    {
		found_filename = check_file_exists ((gchar*)elem->data, filename);
		if (found_filename)
			break;
		elem = elem->next;
    }

	/* If we haven't found the pixmap, try the source directory. */
	if (!found_filename)
    {
		found_filename = check_file_exists ("pixmaps", filename);
    }

	if (!found_filename)
    {
		g_print ("Couldn't find pixmap file: %s", filename);
		return NULL;
    }

	colormap = gtk_widget_get_colormap (widget);
	gdkpixmap = gdk_pixmap_colormap_create_from_xpm (NULL, colormap, &mask,
													 NULL, found_filename);
	g_free (found_filename);
	if (gdkpixmap == NULL)
		return NULL;
	pixmap = gtk_pixmap_new (gdkpixmap, mask);
	gdk_pixmap_unref (gdkpixmap);
	gdk_bitmap_unref (mask);
	return pixmap;
}

GtkWidget*
create_sendWin ()
{
	GtkWidget *sendWin;
	GtkWidget *sendWinEv;
	GtkWidget *vbox1;
	GtkWidget *vpaned1;
	GtkWidget *hbox1;
	GtkWidget *scrolledwindow6;
	GtkWidget *sendUserList;
	GtkWidget *sendUserListNick;
	GtkWidget *sendUserListUser;
	GtkWidget *sendUserListMachine;
	GtkWidget *sendUserListGroup;
	GtkWidget *sendUserListIP;
	GtkWidget *vbox2;
	GtkWidget *viewport1;
	GtkWidget *sendUserNum;
	GtkWidget *hbox2;
	GtkWidget *sendDohoTB;
	GtkWidget *sendRefreshB;
	//GtkWidget *sendGroup;
	//GtkWidget *sendGroup_menu;
	//GtkWidget *glade_menuitem;
	//GtkWidget *hseparator5;
	GtkWidget *scrolledwindow2;
	GtkWidget *sendTextBox;
	GtkWidget *hbox3;
	GtkWidget *sendSendB;
	GtkWidget *sendFushoTB;
	GtkWidget *sendKeyTB;
	GtkAccelGroup *accel_group;

	sendWin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendWin", sendWin);
	gtk_widget_set_usize (sendWin, 430, 500);
	gtk_signal_connect (GTK_OBJECT (sendWin), "destroy",
						GTK_SIGNAL_FUNC (on_sendWin_destroy),
						NULL);
	
	gtk_window_set_title (GTK_WINDOW (sendWin), _("Send Message"));
	//gtk_window_position (GTK_WINDOW (sendWin), GTK_WIN_POS_MOUSE);
	gtk_window_set_policy (GTK_WINDOW (sendWin), TRUE, TRUE, FALSE);

	sendWinEv = gtk_event_box_new();
	gtk_object_set_data(GTK_OBJECT(sendWin), "sendWinEv", sendWinEv);
	gtk_widget_show(sendWinEv);
	gtk_container_add(GTK_CONTAINER(sendWin), sendWinEv);
	gtk_signal_connect (GTK_OBJECT (sendWinEv), "button_press_event",
						GTK_SIGNAL_FUNC (on_sendWinEv_button_press_event),
						NULL);

	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (sendWin), "vbox1", vbox1);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (sendWinEv), vbox1);
	//gtk_paned_pack2 (GTK_PANED (vpaned1), vbox1, TRUE, TRUE);

	vpaned1 = gtk_vpaned_new();
	gtk_object_set_data (GTK_OBJECT (sendWin), "vpaned1", vpaned1);
	gtk_paned_set_gutter_size(GTK_PANED (vpaned1), 8);
	gtk_paned_set_position(GTK_PANED (vpaned1), 320);
	gtk_widget_show(vpaned1);
	//gtk_container_add (GTK_CONTAINER (sendWin), vpaned1);
	gtk_box_pack_start (GTK_BOX (vbox1), vpaned1, TRUE, TRUE, 0);

	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (sendWin), "hbox1", hbox1);
	gtk_widget_show (hbox1);
	//gtk_box_pack_start (GTK_BOX (vbox1), hbox1, TRUE, TRUE, 0);
	gtk_paned_pack1 (GTK_PANED (vpaned1), hbox1, TRUE, TRUE);

	scrolledwindow6 = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (sendWin), "scrolledwindow6", scrolledwindow6);
	gtk_widget_show (scrolledwindow6);
	gtk_box_pack_start (GTK_BOX (hbox1), scrolledwindow6, TRUE, TRUE, 0);

	sendUserList = gtk_clist_new (5);
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserList", sendUserList);
	gtk_widget_show (sendUserList);
	gtk_container_add (GTK_CONTAINER (scrolledwindow6), sendUserList);
	gtk_signal_connect (GTK_OBJECT (sendUserList), "click_column",
						GTK_SIGNAL_FUNC (on_sendUserList_click_column),
						sendWin);
	gtk_signal_connect (GTK_OBJECT (sendUserList), "button_press_event",
						GTK_SIGNAL_FUNC (on_sendUserList_button_press_event),
						sendUserList);
	gtk_signal_connect (GTK_OBJECT (sendWin), "key_press_event",
						GTK_SIGNAL_FUNC (on_sendWin_key_press_event),
						sendUserList);
	accel_group = gtk_accel_group_new ();
	gtk_window_add_accel_group (GTK_WINDOW (sendWin), accel_group);
	gtk_widget_add_accelerator (sendUserList, "select_all", accel_group,
								GDK_A, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_add_accelerator (sendUserList, "unselect_all", accel_group,
								GDK_U, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_add_accelerator (sendUserList, "grab_focus", accel_group,
								GDK_L, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_clist_set_column_width (GTK_CLIST (sendUserList), 0, 80);
	gtk_clist_set_column_width (GTK_CLIST (sendUserList), 1, 80);
	gtk_clist_set_column_width (GTK_CLIST (sendUserList), 2, 80);
	gtk_clist_set_column_width (GTK_CLIST (sendUserList), 3, 80);
	gtk_clist_set_column_width (GTK_CLIST (sendUserList), 4, 80);
	gtk_clist_set_selection_mode (GTK_CLIST (sendUserList), GTK_SELECTION_EXTENDED);
	gtk_clist_column_titles_show (GTK_CLIST (sendUserList));

	sendUserListNick = gtk_label_new (_("Nick"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserListNick", sendUserListNick);
	gtk_widget_show (sendUserListNick);
	gtk_clist_set_column_widget (GTK_CLIST (sendUserList), 0, sendUserListNick);
	gtk_clist_set_column_auto_resize(GTK_CLIST (sendUserList), 0, 1);

	sendUserListUser = gtk_label_new (_("User"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserListUser", sendUserListUser);
	gtk_widget_show (sendUserListUser);
	gtk_clist_set_column_widget (GTK_CLIST (sendUserList), 1, sendUserListUser);
	gtk_clist_set_column_auto_resize(GTK_CLIST (sendUserList), 1, 1);

	sendUserListMachine = gtk_label_new (_("Machine"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserListMachine", sendUserListMachine);
	gtk_widget_show (sendUserListMachine);
	gtk_clist_set_column_widget (GTK_CLIST (sendUserList), 2, sendUserListMachine);
	gtk_clist_set_column_auto_resize(GTK_CLIST (sendUserList), 2, 1);

	sendUserListGroup = gtk_label_new (_("Group"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserListGroup", sendUserListGroup);
	gtk_widget_show (sendUserListGroup);
	gtk_clist_set_column_widget (GTK_CLIST (sendUserList), 3, sendUserListGroup);
	gtk_clist_set_column_auto_resize(GTK_CLIST (sendUserList), 3, 1);

	sendUserListIP = gtk_label_new (_("IP Address"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserListIP", sendUserListIP);
	gtk_widget_show (sendUserListIP);
	gtk_clist_set_column_widget (GTK_CLIST (sendUserList), 4, sendUserListIP);
	gtk_clist_set_column_auto_resize(GTK_CLIST (sendUserList), 4, 1);

	gtk_clist_set_sort_type(GTK_CLIST(sendUserList), GTK_SORT_ASCENDING);
	gtk_clist_set_sort_column(GTK_CLIST(sendUserList), 0);
	gtk_clist_set_auto_sort(GTK_CLIST(sendUserList), 1);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (sendWin), "vbox2", vbox2);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (hbox1), vbox2, FALSE, TRUE, 8);

	viewport1 = gtk_viewport_new (0, 0);
	gtk_widget_show (viewport1);
	gtk_box_pack_start (GTK_BOX (vbox2), viewport1, FALSE, TRUE, 20);
	gtk_viewport_set_shadow_type(GTK_VIEWPORT(viewport1), GTK_SHADOW_IN);
	
	sendUserNum = gtk_label_new (""); /* �桼������ */
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendUserNum", sendUserNum);
	gtk_widget_show (sendUserNum);
	//gtk_box_pack_start (GTK_BOX (vbox2), sendUserNum, FALSE, TRUE, 20);
	gtk_container_add (GTK_CONTAINER (viewport1), sendUserNum);

	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (sendWin), "hbox2", hbox2);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox2, FALSE, FALSE, 0);

	sendDohoTB = gtk_check_button_new_with_label (_("Broadcast"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendDohoTB", sendDohoTB);
	gtk_widget_show (sendDohoTB);
	gtk_box_pack_start (GTK_BOX (hbox2), sendDohoTB, TRUE, FALSE, 0);
	gtk_signal_connect (GTK_OBJECT (sendDohoTB), "toggled",
						GTK_SIGNAL_FUNC (on_sendDohoTB_toggled),
						sendUserList);
	gtk_widget_add_accelerator (sendDohoTB, "clicked", accel_group,
								GDK_O, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	sendRefreshB = gtk_button_new_with_label (_("Refresh"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendRefreshB", sendRefreshB);
	gtk_widget_show (sendRefreshB);
	gtk_box_pack_start (GTK_BOX (hbox2), sendRefreshB, TRUE, TRUE, 6);
	gtk_signal_connect (GTK_OBJECT (sendRefreshB), "clicked",
						GTK_SIGNAL_FUNC (on_sendRefreshB_clicked),
						sendWin);
	gtk_widget_add_accelerator (sendRefreshB, "clicked", accel_group,
								GDK_R, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (sendWin), "scrolledwindow2", scrolledwindow2);
	gtk_widget_show (scrolledwindow2);
	//gtk_box_pack_start (GTK_BOX (vbox1), scrolledwindow2, FALSE, TRUE, 0);
	gtk_paned_pack2 (GTK_PANED (vpaned1), scrolledwindow2, TRUE, TRUE);
	//gtk_widget_set_usize (scrolledwindow2, -2, 124);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

	sendTextBox = gtk_text_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendTextBox", sendTextBox);
	gtk_widget_show (sendTextBox);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), sendTextBox);
	//gtk_widget_set_usize (sendTextBox, -2, 115);
	gtk_widget_add_accelerator (sendTextBox, "grab_focus", accel_group,
								GDK_G, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_text_set_editable (GTK_TEXT (sendTextBox), TRUE);

	hbox3 = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (sendWin), "hbox3", hbox3);
	gtk_widget_show (hbox3);
	gtk_box_pack_start (GTK_BOX (vbox1), hbox3, FALSE, FALSE, 0);

	sendSendB = gtk_button_new_with_label (_("Send"));
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendSendB", sendSendB);
	gtk_widget_show (sendSendB);
	gtk_box_pack_start (GTK_BOX (hbox3), sendSendB, TRUE, TRUE, 6);
	gtk_signal_connect (GTK_OBJECT (sendSendB), "clicked",
						GTK_SIGNAL_FUNC (on_sendSendB_clicked),
						sendWin);
	gtk_widget_add_accelerator (sendSendB, "clicked", accel_group,
								GDK_S, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	sendFushoTB = gtk_check_button_new_with_label (_("Seal")); /* ���� */
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendFushoTB", sendFushoTB);
	gtk_widget_show (sendFushoTB);
	gtk_box_pack_start (GTK_BOX (hbox3), sendFushoTB, FALSE, FALSE, 0);
	gtk_widget_add_accelerator (sendFushoTB, "toggled", accel_group,
								GDK_Z, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(sendFushoTB), option.seal);

	sendKeyTB = gtk_check_button_new_with_label (_("Lock")); /* �� */
	gtk_object_set_data (GTK_OBJECT (sendWin), "sendKeyTB", sendKeyTB);
	gtk_widget_show (sendKeyTB);
	gtk_box_pack_start (GTK_BOX (hbox3), sendKeyTB, FALSE, FALSE, 0);
	gtk_widget_add_accelerator (sendKeyTB, "toggled", accel_group,
								GDK_Y, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

#if 1
	gtk_widget_add_accelerator (sendWin, "destroy", accel_group,
								GDK_Q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
#endif
	gtk_widget_grab_focus(get_widget(GTK_WIDGET(sendWin), "sendTextBox")); /* focus��ܤ� */
	winSend = sendWin;
	return sendWin;
}

GtkWidget*
create_recvWin ()
{
	GtkWidget *recvWin;
	GtkWidget *vbox3;
	GtkWidget *frame6;
	GtkWidget *recvMsg;
	GtkWidget *recvWinScrollbar;
	GtkWidget *recvText;
	GtkWidget *vpaned1;
	GtkWidget *recvWinKaifu;
	GtkWidget *recvWinButtonBox;
	GtkWidget *recvCloseB;
	GtkWidget *recvSendB;
	GtkWidget *recvWinCite;
	GtkAccelGroup *accel_group;
	GtkWidget *recvWinURLScrollbar;
	GtkWidget *recvWinURLBox;

	recvWin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWin", recvWin);
	gtk_widget_set_usize (recvWin, 300, 300);
	gtk_signal_connect (GTK_OBJECT (recvWin), "destroy",
						GTK_SIGNAL_FUNC (on_recvWin_destroy),
						NULL);
	gtk_window_set_title (GTK_WINDOW (recvWin), _("Receive Message"));
	gtk_window_set_policy (GTK_WINDOW (recvWin), TRUE, TRUE, FALSE);

	vbox3 = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (recvWin), "vbox3", vbox3);
	gtk_widget_show (vbox3);
	gtk_container_add (GTK_CONTAINER (recvWin), vbox3);

	frame6 = gtk_frame_new (_("Message From..."));
	gtk_object_set_data (GTK_OBJECT (recvWin), "frame6", frame6);
	gtk_widget_show (frame6);
	//gtk_box_pack_start (GTK_BOX (vbox3), frame6, TRUE, TRUE, 6);
	gtk_box_pack_start (GTK_BOX (vbox3), frame6, FALSE, FALSE, 6);
	gtk_container_border_width (GTK_CONTAINER (frame6), 2);
	gtk_frame_set_label_align (GTK_FRAME (frame6), 0.06, 0.5);

	recvMsg = gtk_label_new ("message...");
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvMsg", recvMsg);
	gtk_widget_show (recvMsg);
	gtk_container_add (GTK_CONTAINER (frame6), recvMsg);
	gtk_misc_set_padding (GTK_MISC (recvMsg), 0, 8);

	vpaned1 = gtk_vpaned_new();
	gtk_object_set_data (GTK_OBJECT (recvWin), "vpaned1", vpaned1);
	gtk_paned_set_gutter_size(GTK_PANED (vpaned1), 4);
	//gtk_paned_set_position(GTK_PANED (vpaned1), 320);
	gtk_widget_show(vpaned1);
	gtk_box_pack_start (GTK_BOX (vbox3), vpaned1, TRUE, TRUE, 0);

	recvWinURLScrollbar = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinURLScrollbar", recvWinURLScrollbar);
	//gtk_widget_show (recvWinURLScrollbar);
	//gtk_box_pack_start (GTK_BOX (vbox3), recvWinURLScrollbar, FALSE, FALSE, 0);
	gtk_paned_pack1 (GTK_PANED (vpaned1), recvWinURLScrollbar, TRUE, TRUE);
	//gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (recvWinURLScrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (recvWinURLScrollbar), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	//gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (recvWinURLScrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	recvWinURLBox = gtk_vbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinURLBox", recvWinURLBox);
	gtk_widget_show (recvWinURLBox);
	//gtk_container_add (GTK_CONTAINER (recvWinURLScrollbar), recvWinURLBox);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(recvWinURLScrollbar), recvWinURLBox);

	recvWinScrollbar = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinScrollbar", recvWinScrollbar);
	gtk_widget_show (recvWinScrollbar);
	//gtk_box_pack_start (GTK_BOX (vbox3), recvWinScrollbar, TRUE, TRUE, 0);
	gtk_paned_pack2 (GTK_PANED (vpaned1), recvWinScrollbar, TRUE, TRUE);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (recvWinScrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

	recvText = gtk_text_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvText", recvText);
	gtk_widget_show (recvText);
	gtk_container_add (GTK_CONTAINER (recvWinScrollbar), recvText);
	accel_group = gtk_accel_group_new ();
	gtk_window_add_accel_group (GTK_WINDOW (recvWin), accel_group);
	gtk_widget_add_accelerator (recvText, "grab_focus", accel_group,
								GDK_G, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	recvWinKaifu = gtk_button_new_with_label (_("Open")); /* ���� */
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinKaifu", recvWinKaifu);
	gtk_box_pack_start (GTK_BOX (vbox3), recvWinKaifu, TRUE, TRUE, 0);
	gtk_widget_set_usize (recvWinKaifu, -2, 146);
	gtk_signal_connect (GTK_OBJECT (recvWinKaifu), "clicked",
						GTK_SIGNAL_FUNC (on_recvWinKaifu_clicked),
						recvWin);
	gtk_widget_add_accelerator (recvWinKaifu, "clicked", accel_group,
								GDK_space, 0, GTK_ACCEL_VISIBLE);

	recvWinButtonBox = gtk_hbox_new (FALSE, 0);
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinButtonBox", recvWinButtonBox);
	gtk_widget_show (recvWinButtonBox);
	gtk_box_pack_start (GTK_BOX (vbox3), recvWinButtonBox, FALSE, FALSE, 0);

	recvCloseB = gtk_button_new_with_label (_("Close")); /* �Ĥ��� */
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvCloseB", recvCloseB);
	gtk_widget_show (recvCloseB);
	gtk_box_pack_start (GTK_BOX (recvWinButtonBox), recvCloseB, TRUE, TRUE, 8);
	gtk_signal_connect_object (GTK_OBJECT (recvCloseB), "clicked",
							   GTK_SIGNAL_FUNC (gtk_widget_destroy),
							   GTK_OBJECT (recvWin));
	gtk_widget_add_accelerator (recvCloseB, "clicked", accel_group,
								GDK_Q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	recvSendB = gtk_button_new_with_label (_("Reply"));	/* �޷� */
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvSendB", recvSendB);
	gtk_widget_show (recvSendB);
	gtk_box_pack_start (GTK_BOX (recvWinButtonBox), recvSendB, TRUE, TRUE, 6);
	gtk_signal_connect (GTK_OBJECT (recvSendB), "clicked",
						GTK_SIGNAL_FUNC (on_recvSend_clicked),
						recvWin);
	gtk_widget_add_accelerator (recvSendB, "clicked", accel_group,
								GDK_S, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	recvWinCite = gtk_check_button_new_with_label (_("Cite")); /* ���� */
	gtk_object_set_data (GTK_OBJECT (recvWin), "recvWinCite", recvWinCite);
	gtk_widget_show (recvWinCite);
	gtk_box_pack_start (GTK_BOX (recvWinButtonBox), recvWinCite, FALSE, FALSE, 0);
	gtk_widget_add_accelerator (recvWinCite, "toggled", accel_group,
								GDK_C, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(recvWinCite), option.cite);

	return recvWin;
}

GtkWidget*
create_logWin ()
{
	GtkWidget *logWin;
	GtkWidget *scrolledwindow5;
	GtkWidget *logWinLog;

	logWin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (logWin), "logWin", logWin);
	gtk_widget_set_usize (logWin, 600, 300);
	gtk_signal_connect (GTK_OBJECT (logWin), "delete_event",
						GTK_SIGNAL_FUNC (on_logWin_delete_event),
						NULL);
	gtk_window_set_title (GTK_WINDOW (logWin), "gipmsg - log window");
	gtk_window_set_policy (GTK_WINDOW (logWin), TRUE, TRUE, FALSE);

	scrolledwindow5 = gtk_scrolled_window_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (logWin), "scrolledwindow5", scrolledwindow5);
	gtk_widget_show (scrolledwindow5);
	gtk_container_add (GTK_CONTAINER (logWin), scrolledwindow5);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow5), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	logWinLog = gtk_text_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (logWin), "logWinLog", logWinLog);
	gtk_widget_show (logWinLog);
	gtk_container_add (GTK_CONTAINER (scrolledwindow5), logWinLog);

	return logWin;
}


#ifdef APPLET
void AppletRegistAbsence(GtkWidget *app)
{
	gint		i;
	gchar		absence_buf[32];
	
	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		sprintf(absence_buf, "absence%d", i+1);
		applet_widget_register_callback (APPLET_WIDGET(app),
										 absence_buf, option.absence[i],
										 (AppletCallbackFunc)on_popupmenu_fuzai_applet_cb, (gpointer)(i+1));
	}
	applet_widget_register_stock_callback (APPLET_WIDGET(app),
										   "absence0", GNOME_STOCK_MENU_STOP, _("< cancel >"),
										   (AppletCallbackFunc)on_popupmenu_fuzai_applet_cb, (gpointer)0);
}

void AppletUnregistAbsence(GtkWidget *app)
{
	gint		i;
	gchar		absence_buf[32];

	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		sprintf(absence_buf, "absence%d", i+1);
		applet_widget_unregister_callback (APPLET_WIDGET(app), absence_buf);
	}
	applet_widget_unregister_callback (APPLET_WIDGET(app), "absence0");
}
#endif /* APPLET */

#ifdef APPLET
#  include "../ipmsg_a.xpm"
#  include "../ipmsgrev_a.xpm"
#endif
#include "../ipmsg.xpm"
#include "../ipmsgrev.xpm"
GtkWidget* create_buttonWin(GtkWidget *app)
{
	GtkWidget *buttonWin;
	GtkWidget *buttonWinEv;
	GtkWidget *box;
	GtkWidget *buttonWinPix;
	GtkWidget *buttonWinPix2;
	GtkTooltips *tooltips;

	tooltips = gtk_tooltips_new ();

	//buttonWin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	buttonWin = app;
	gtk_object_set_data (GTK_OBJECT (buttonWin), "buttonWin", buttonWin);
	gtk_tooltips_set_tip (tooltips, buttonWin, "GIPMSG", NULL);
	if (!applet_p())
		gtk_window_set_wmclass (GTK_WINDOW (buttonWin), "", "GipmsgButton");

	buttonWinEv = gtk_event_box_new ();
	gtk_object_set_data (GTK_OBJECT (buttonWin), "buttonWinEv", buttonWinEv);
#ifdef APPLET
	if (applet_p())
		applet_widget_add(APPLET_WIDGET(app), buttonWinEv);
	else
#endif /* APPLET */
		gnome_app_set_contents(GNOME_APP(app), buttonWinEv);
	gtk_signal_connect (GTK_OBJECT (buttonWinEv), "button_press_event",
						GTK_SIGNAL_FUNC (on_buttonWinEv_button_press_event),
						NULL);
	gtk_signal_connect (GTK_OBJECT (buttonWin), "key_press_event",
						GTK_SIGNAL_FUNC (on_buttonWinEv_key_press_event),
						NULL);

	box = gtk_hbox_new(FALSE, 0);
	gtk_object_set_data(GTK_OBJECT(buttonWin), "vbox", box);
	gtk_container_add (GTK_CONTAINER (buttonWinEv), box);

#ifdef APPLET
	if (applet_p())
		buttonWinPix = gnome_pixmap_new_from_xpm_d((char**)ipmsg_a_xpm);
	else
#endif /* APPLET */
		buttonWinPix = gnome_pixmap_new_from_xpm_d((char**)ipmsg_xpm);
	if (buttonWinPix == NULL)
		g_error (_("Couldn't create pixmap"));
	gtk_object_set_data (GTK_OBJECT (buttonWin), "buttonWinPix", buttonWinPix);
//	gtk_container_add (GTK_CONTAINER (buttonWinEv), buttonWinPix);
	gtk_box_pack_start(GTK_BOX(box), buttonWinPix, TRUE, TRUE, 0);

#ifdef APPLET
	if (applet_p())
		buttonWinPix2 = gnome_pixmap_new_from_xpm_d((char**)ipmsgrev_a_xpm);
	else
#endif /* APPLET */
		buttonWinPix2 = gnome_pixmap_new_from_xpm_d((char**)ipmsgrev_xpm);
	if (buttonWinPix2 == NULL)
		g_error (_("Couldn't create pixmap"));
	gtk_object_set_data(GTK_OBJECT (buttonWin), "buttonWinPix2", buttonWinPix2);
//	gtk_container_add (GTK_CONTAINER (buttonWinEv), buttonWinPix2);
	gtk_box_pack_start(GTK_BOX(box), buttonWinPix2, TRUE, TRUE, 0);

	//GTK_WIDGET_SET_FLAGS (buttonWinPix, GTK_CAN_FOCUS);
	//gtk_misc_set_padding (GTK_MISC (buttonWinPix), 2, 2);

	gtk_object_set_data (GTK_OBJECT (buttonWin), "tooltips", tooltips);

#ifdef APPLET
	if (applet_p()) {
		/*===== set applet signals =====*/
		applet_widget_register_stock_callback(APPLET_WIDGET(app),
											  "about", GNOME_STOCK_MENU_ABOUT, _("About..."),
											  (AppletCallbackFunc)on_mainmenu_about_cb, NULL);
		applet_widget_register_stock_callback(APPLET_WIDGET(app),
											  "properties", GNOME_STOCK_MENU_PREF, _("Properties..."),
											  (AppletCallbackFunc)on_mainmenu_setting_cb, NULL);
#if 0
		applet_widget_register_stock_callback(APPLET_WIDGET(app),
											  "show_log", GNOME_STOCK_MENU_EXEC, _("Show Log"),
											  (AppletCallbackFunc)on_mainmenu_show_log_cb, NULL);
#endif
		AppletRegistAbsence(app);
	}
#endif /* APPLET */
	return buttonWin;
}

GtkWidget*
create_kaifuWin ()
{
	GtkWidget *kaifuWin;
	GtkWidget *dialog_vbox2;
	GtkWidget *kaifuWinMsg;
	GtkWidget *dialog_action_area2;
	GtkWidget *kaifuWinOK;
	GtkAccelGroup *accel_group;

	kaifuWin = gtk_dialog_new ();
	gtk_object_set_data (GTK_OBJECT (kaifuWin), "kaifuWin", kaifuWin);
	gtk_window_set_policy (GTK_WINDOW (kaifuWin), TRUE, TRUE, FALSE);

	dialog_vbox2 = GTK_DIALOG (kaifuWin)->vbox;
	gtk_object_set_data (GTK_OBJECT (kaifuWin), "dialog_vbox2", dialog_vbox2);
	gtk_widget_show (dialog_vbox2);

	kaifuWinMsg = gtk_label_new ("  ");
	gtk_object_set_data (GTK_OBJECT (kaifuWin), "kaifuWinMsg", kaifuWinMsg);
	gtk_widget_show (kaifuWinMsg);
	gtk_box_pack_start (GTK_BOX (dialog_vbox2), kaifuWinMsg, TRUE, TRUE, 8);
	GTK_WIDGET_SET_FLAGS (kaifuWinMsg, GTK_CAN_FOCUS);
	gtk_widget_grab_focus (kaifuWinMsg);

	dialog_action_area2 = GTK_DIALOG (kaifuWin)->action_area;
	gtk_object_set_data (GTK_OBJECT (kaifuWin), "dialog_action_area2", dialog_action_area2);
	gtk_widget_show (dialog_action_area2);
	gtk_container_border_width (GTK_CONTAINER (dialog_action_area2), 3);

	kaifuWinOK = gtk_button_new_with_label (_("OK"));
	gtk_object_set_data (GTK_OBJECT (kaifuWin), "kaifuWinOK", kaifuWinOK);
	gtk_widget_show (kaifuWinOK);
	gtk_box_pack_start (GTK_BOX (dialog_action_area2), kaifuWinOK, TRUE, TRUE, 0);
	GTK_WIDGET_SET_FLAGS (kaifuWinOK, GTK_CAN_DEFAULT);
	gtk_signal_connect_object (GTK_OBJECT (kaifuWinOK), "clicked",
							   GTK_SIGNAL_FUNC (gtk_widget_destroy),
							   GTK_OBJECT (kaifuWin));
	accel_group = gtk_accel_group_new ();
	gtk_window_add_accel_group (GTK_WINDOW (kaifuWin), accel_group);
	gtk_widget_add_accelerator (kaifuWinOK, "clicked", accel_group,
								GDK_Q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	return kaifuWin;
}

GtkWidget*
create_popupmenu_fuzai(void)
{
	GtkWidget	*menu;
	GtkWidget	*item;
	GtkWidget	*root_menu;
	gint	  	i;
	static gint count = 0;
	gchar	   	tbuf[32];

	menu = gtk_menu_new();

	sprintf(tbuf, "count: %d", count);
    item = gtk_menu_item_new_with_label(tbuf);
    gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
	g_print("%s\n", tbuf);
	for (i = 0; i < IPMSG_ABSENCE_NUM; i++) {
		item = gtk_menu_item_new_with_label(option.absence[i]);
		gtk_menu_append(GTK_MENU(menu), item);
		gtk_signal_connect_object( GTK_OBJECT(item), "activate",
								   GTK_SIGNAL_FUNC(on_popupmenu_fuzai_cb), (gpointer)(i+1));
		gtk_widget_show(item);
	}
    item = gtk_menu_item_new_with_label(_("< cancel >"));
    gtk_menu_append(GTK_MENU(menu), item);
    gtk_signal_connect_object( GTK_OBJECT(item), "activate",
                               GTK_SIGNAL_FUNC(on_popupmenu_fuzai_cb), (gpointer)0);
    gtk_widget_show(item);

    root_menu = gtk_menu_item_new_with_label("Root Menu");
    gtk_widget_show(root_menu);
    gtk_menu_item_set_submenu( GTK_MENU_ITEM(root_menu), menu );
	return menu;
}


GtkWidget*
create_menu_select_group(GtkWidget* sendUserList)
{
	GtkWidget	*menu;
	GtkWidget	*item;
	GtkWidget	*root_menu;
	GList		*glist = NULL, *p;

	glist = IPMsgGetGroupList();
	menu = gtk_menu_new();
	for (p = glist; p != NULL; p = p->next) {
		item = gtk_menu_item_new_with_label(p->data);
		gtk_menu_append(GTK_MENU(menu), item);
		gtk_signal_connect( GTK_OBJECT(item), "activate",
								   GTK_SIGNAL_FUNC(on_menu_select_group_cb), (gpointer)p->data);
		gtk_widget_show(item);
	}
    root_menu = gtk_menu_item_new_with_label("Root Menu");
    gtk_widget_show(root_menu);
    gtk_menu_item_set_submenu( GTK_MENU_ITEM(root_menu), menu );

	menu_select_group_sendUserList = sendUserList;

	g_list_free(glist);

	return menu;
}


GtkWidget*
create_mainmenu(void)
{
	GnomeUIInfo mainmenu[] = {
		{GNOME_APP_UI_ITEM, 
		 _("Properties..."), _("Properties of this program"),
		 on_mainmenu_setting_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
		 0, 0, NULL},
#if 0
		{GNOME_APP_UI_ITEM, 
		 _("Show Log"), _("show log"),
		 on_mainmenu_show_log_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXEC,
		 0, 0, NULL},
#endif
		{GNOME_APP_UI_ITEM, 
		 _("About..."), _("Info about this program"),
		 on_mainmenu_about_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
		 0, 0, NULL},
		GNOMEUIINFO_SEPARATOR,
//		GNOMEUIINFO_HELP("help-browser"),
//		GNOMEUIINFO_SEPARATOR,
		{GNOME_APP_UI_ITEM, 
		 _("Quit..."), _("Quit from program"),
		 on_buttonWin_destroy, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
		 0, 0, NULL},
		GNOMEUIINFO_END
	};

	return gnome_popup_menu_new(mainmenu);
}


#if 0
GtkWidget*
create_geometry_menu(gint n)
{
	GnomeUIInfo menu[] = {
		{GNOME_APP_UI_ITEM, 
		 _("Save position"), _("Save posiion"),
		 on_mainmenu_setting_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
		 0, 0, NULL},
		{GNOME_APP_UI_ITEM, 
		 _("Forget position"), _("Forget posiion"),
		 on_mainmenu_setting_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
		 0, 0, NULL},
		{GNOME_APP_UI_ITEM, 
		 _("Save width & height"), _("Save width & height"),
		 on_mainmenu_setting_cb, NULL, NULL, 
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
		 0, 0, NULL},
		{GNOME_APP_UI_ITEM, 
		_("Forget width & height"), _("Forget width & height"),
		on_mainmenu_setting_cb, NULL, NULL, 
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
		0, 0, NULL},
		GNOMEUIINFO_END
	};

	return gnome_popup_menu_new(menu);
}
#endif


GtkWidget* create_retry_win(IPMsgSentMsg *smsg)
{
	GtkWidget	*dialog;
	GtkWidget	*widget;
	gchar		*buf;

	//dialog = gnome_dialog_new("GIPMSG - resend", N_("retry"), N_("cancel"), NULL);
	dialog = gnome_dialog_new(_("GIPMSG - resend?"), GNOME_STOCK_BUTTON_NO, GNOME_STOCK_BUTTON_YES, NULL);
	buf = g_strdup_printf(_("Cannot send message to %s(%s).\n Do you retry?"), smsg->user, smsg->machine);
	widget = gtk_label_new(buf);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox), widget, TRUE, TRUE, 0);
	widget = gtk_hseparator_new();
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox), widget, FALSE, FALSE, 4);
	widget = gtk_text_new(NULL, NULL);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox), widget, TRUE, TRUE, 0);
	g_free(buf);
	buf = g_malloc(strstoelen(smsg->msg));
	strstoe(buf, smsg->msg);
	gtk_text_set_editable(GTK_TEXT(widget), 0);
	gtk_text_freeze(GTK_TEXT(widget));
	gtk_text_insert(GTK_TEXT(widget), NULL, NULL, NULL, buf, -1);
	gtk_text_thaw(GTK_TEXT(widget));
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, on_retryWin_cancel_clicked, smsg);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, on_retryWin_retry_clicked, smsg);
	g_free(buf);

	return dialog;
}
