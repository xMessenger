/*	@(#)Copyright (C) H.Shirouzu 1996-1998   ipmsg.h	Ver1.34 */

#ifndef IPMSG_H
#define IPMSG_H

#include "db.h"
#include <netinet/in.h>

/*  IP Messenger Communication Protocol version 1.0 define  */
/*  macro  */
#define GET_MODE(command)	(command & 0x000000ffUL)
#define GET_OPT(command)	(command & 0xffffff00UL)

/*  header  */
#define IPMSG_VERSION			0x0001
#define IPMSG_DEFAULT_PORT		0x0979

/*  command  */
#define IPMSG_NOOPERATION		0x00000000UL

#define IPMSG_BR_ENTRY			0x00000001UL
#define IPMSG_BR_EXIT			0x00000002UL
#define IPMSG_ANSENTRY			0x00000003UL
#define IPMSG_BR_ABSENCE		0x00000004UL

#define IPMSG_BR_ISGETLIST		0x00000010UL
#define IPMSG_OKGETLIST			0x00000011UL
#define IPMSG_GETLIST			0x00000012UL
#define IPMSG_ANSLIST			0x00000013UL
#define IPMSG_BR_ISGETLIST2		0x00000018UL

#define IPMSG_SENDMSG			0x00000020UL
#define IPMSG_RECVMSG			0x00000021UL
#define IPMSG_READMSG			0x00000030UL
#define IPMSG_DELMSG			0x00000031UL

#define IPMSG_GETINFO			0x00000040UL
#define IPMSG_SENDINFO			0x00000041UL

#define IPMSG_GETABSENCEINFO	0x00000050UL
#define IPMSG_SENDABSENCEINFO	0x00000051UL

/*  option for all command  */
#define IPMSG_ABSENCEOPT		0x00000100UL
#define IPMSG_SERVEROPT			0x00000200UL
#define IPMSG_DIALUPOPT			0x00010000UL

/*  option for send command  */
#define IPMSG_SENDCHECKOPT		0x00000100UL
#define IPMSG_SECRETOPT			0x00000200UL
#define IPMSG_BROADCASTOPT		0x00000400UL
#define IPMSG_MULTICASTOPT		0x00000800UL
#define IPMSG_NOPOPUPOPT		0x00001000UL
#define IPMSG_AUTORETOPT		0x00002000UL
#define IPMSG_RETRYOPT			0x00004000UL
#define IPMSG_PASSWORDOPT		0x00008000UL
#define IPMSG_NOLOGOPT			0x00020000UL
#define IPMSG_NEWMUTIOPT		0x00040000UL
#define IPMSG_NOADDLISTOPT		0x00080000UL

#define HOSTLIST_DELIMIT	"\a"
#define HOSTLIST_DUMMY		"\b"

/*  end of IP Messenger Communication Protocol version 1.0 define  */


/*  IP Messenger for Windows  internal define  */
#define IPMSG_REVERSEICON			0x0100
#define IPMSG_TIMERINTERVAL			4000
#define IPMSG_CLOCKTICK				500
#define IPMSG_GETLIST_FINISH		0

#define IPMSG_BROADCAST_TIMER		0x0101
#define IPMSG_SEND_TIMER			0x0102
#define IPMSG_DELETE_TIMER			0x0103
#define IPMSG_LISTGET_TIMER			0x0104
#define IPMSG_LISTGETRETRY_TIMER	0x0105
#define IPMSG_ENTRY_TIMER			0x0106
#define IPMSG_DUMMY_TIMER			0x0107

#define	IPMSG_RETRY_TIMES		4

#define IPMSG_NICKNAME			1
#define IPMSG_FULLNAME			2

#define IPMSG_NAMESORT			0x00000000
#define IPMSG_IPADDRSORT		0x00000001
#define IPMSG_HOSTSORT			0x00000002
#define IPMSG_NOGROUPSORTOPT	0x00000100
#define IPMSG_ICMPSORTOPT		0x00000200
#define IPMSG_NOKANJISORTOPT	0x00000400
#define IPMSG_ALLREVSORTOPT		0x00000800
#define IPMSG_GROUPREVSORTOPT	0x00001000
#define IPMSG_SUBREVSORTOPT		0x00002000

#if defined WIN32 || defined XWINDOW
#define MAX_SOCKBUF		32768
#else
#define MAX_SOCKBUF		8192
#endif
#define MAX_UDPBUF		8192
#define MAX_BUF			1024
#define MAX_NAMEBUF		50
#define MAX_LANGBUF		10
#define MAX_LISTBUF		(MAX_NAMEBUF * 3 + 50)

#define HS_TOOLS		"HSTools"
#define IP_MSG			"IPMsg"
#define NO_NAME			"no_name"
#define URL_STR			"://"
#define MAILTO_STR		"mailto:"


typedef struct IPMsgList_T {
	gchar	*nick;
	gchar	*machine;
	gchar	*user;
	gchar	*group;
	guint	recv;
	guint	send;
	gboolean   dialup;
	struct sockaddr_in	from;
} IPMsgList;
typedef struct IPMsgMessage_T {
	struct sockaddr_in	from;
	gint	fromlen;
#if 0
	gchar	serial[32];
	gchar	user[32];
	gchar	machine[32];
	gchar	group[32];
	guint	com, com2;
	gchar	appendix[MAX_SOCKBUF];
#else
	gchar	*serial;
	gchar	*user;
	gchar	*machine;
	gchar	*group;
	guint	com, com2;
	gchar	*appendix;
#endif
} IPMsgMessage;
typedef struct IPMsgSentMsg_T {
	struct DB_DAT_T	*db_tag;
	guint	serial;				/* 送信したときのserialを保存 */
	struct sockaddr_in	to;
#if 0
	gchar	user[32];
	gchar	machine[32];
#else
	gchar	*user;
	gchar	*machine;
#endif
	guint	com;
	gchar	*msg;				/* 再送する為に文字列を保存する */
	gint	timeout_tag;		/* 再送用に設定したコールバックのtag */
	gint	retry;
} IPMsgSentMsg;
typedef struct IPMsgReadMsgData_T {
	struct DB_DAT_T	*db_tag;
	struct sockaddr_in	from;
	guint	serial;				/* serialを保存 */
	gchar	*msg;				/* 再送する為に文字列を保存する */
	GtkWidget	*win;
	unsigned	have_url	:1;
	unsigned	shown		:1;
} IPMsgReadMsgData;


void IPMsgFree_msg(struct IPMsgMessage_T *msg);
void IPMsgFree_list(struct IPMsgList_T *list);
void IPMsgFree_smsg(struct IPMsgSentMsg_T *smsg);
void IPMsgFree_rmsg(struct IPMsgReadMsgData_T *rmsg);


void IPMsgSetup(guint16 port);
void IPMsgQuit(void);
void IPMsgSetBcasts(char *lbcast, char *bcasts);
void IPMsgLogin(void);
void IPMsgMain(gpointer data, gint source, GdkInputCondition condition);
gint IPMsgSend(struct sockaddr_in *dest, guint32 com, gchar *data);
void IPMsgSendBroadcast(guint32 com, gchar *data);
gint IPMsgReSendData(gpointer dat);
void IPMsgFuzaiChange(gint mode);

gchar *IPMsgGetNickWithAbsence(void);


int IPMsgSearchUser(struct DB_DAT_T *dat, void *para);
int IPMsgSearchSerial(struct DB_DAT_T *dat, void *para);
int IPMsgSendWinDraw(struct DB_DAT_T *dat, void *para);
int IPMsgSendData(IPMsgList	*data, void *sendWin, guint32 flag);
int IPMsgSendDataBroadcast(void *sendWin, guint32 flag);

int IPMsgSearchReadMsg(struct DB_DAT_T *dat, void *para);
int IPMsgSearchReadFrom(struct DB_DAT_T *dat, void *para);

int IPMsgSearchNick(struct DB_DAT_T *dat, void *para);

GList *IPMsgGetGroupList(void);


/* for db each */
int IPMsgDBEachFree_list(struct DB_DAT_T *dat, void *para);

/* sort */
gint sendWin_clist_comp(GtkCList *clist,
						gconstpointer ptr1,
						gconstpointer ptr2);
#if 0
int IPMsgUserListSortName(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i);
int IPMsgUserListSortUser(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i);
int IPMsgUserListSortMachine(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i);
int IPMsgUserListSortGroup(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i);
int IPMsgUserListSortIP(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i);
#endif

/* window */
GtkWidget* IPMsgOpenSendWin(void);
void IPMsgResetTooltip(void);

/* non poopup */
void IPMsgOpenNonPopup(void);
gint IPMsgNonPopupTimeout(gpointer dat);


#ifndef IPMSG_C__
extern DB	*db;
extern DB	*db_send;			/* db of sent message */
extern DB	*db_read;			/* db of read message */
extern DB	*db_broadcast;		/* db of broadcast addresses */
extern gint	nIPMsgNonPopup;
#endif /* IPMSG_C__ */

#endif /* IPMSG_H */
