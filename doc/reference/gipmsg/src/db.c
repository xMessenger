#include <stdio.h>
#include <stdlib.h>
#include "db.h"


#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif
#include <gnome.h>
//#include <gtk/gtk.h>
#define	malloc(X...)		g_malloc(##X##)
#define	free(X...)			g_free(##X##)


struct DB_T *db_new(int (*comp)(const void *, const void *))
{
	struct DB_T	*db;

	db = malloc(sizeof(struct DB_T));
	db->num = 0;
	db->top = 0;
	db->end = 0;
	db->comp = comp;
	return db;
}

void db_free(struct DB_T *db)
{
	db_clear(db);
	free(db);
}

void db_clear(struct DB_T *db)
{
	struct DB_DAT_T	*dat;
	struct DB_DAT_T	*next;

	for (dat = db->top; dat != 0; dat = next) {
		next = dat->next;
		dbd_free(db, dat);
	}
}

void db_delete(struct DB_T *db)
{
	struct DB_DAT_T	*dat;
	struct DB_DAT_T	*next;

	for (dat = db->top; dat != 0; dat = next) {
		if (dat->data) free(dat->data);
		next = dat->next;
		dbd_free(db, dat);
	}
}

struct DB_DAT_T *dbd_append(struct DB_T *db, void *data)
{
	struct DB_DAT_T *dat;

	dat = malloc(sizeof(struct DB_DAT_T));
	dat->next = 0;
	dat->prev = db->end;
	if (db->end != 0)	db->end->next = dat;
	if (db->top == 0)	db->top = dat;
	db->end = dat;
	dat->data = data;
	db->num++;
	return dat;
}

void dbd_free(struct DB_T *db, struct DB_DAT_T *dat)
{
	if (dat == db->top)	db->top = dat->next;
	else				dat->prev->next = dat->next;
	if (dat == db->end)	db->end = dat->prev;
	else				dat->next->prev = dat->prev;
	db->num--;
	free(dat);
}

int db_foreach(struct DB_T *db, int (*func)(struct DB_DAT_T	*, void *), void *data)
{
	struct DB_DAT_T	*dat;
	int		ret;

	for (dat = db->top; dat != 0; dat = dat->next) {
		if ((ret = func(dat, data)) != 0)	return ret;
	}
	return 0;
}

int db_count(struct DB_T *db, int (*func)(struct DB_DAT_T	*, void *), void *data)
{
	struct DB_DAT_T	*dat;
	int		count = 0;

	for (dat = db->top; dat != 0; dat = dat->next) {
		if (func(dat, data) != 0)	count++;
	}
	return count;
}


#if 1
int db_bsort(struct DB_T *db, int (*comp)(struct DB_DAT_T *, struct DB_DAT_T *))
{
	struct DB_DAT_T	*dat;
	struct DB_DAT_T	*dat_top;
	struct DB_DAT_T	*dat_next;
	struct DB_DAT_T	*dat_i;
	int		c;
	struct DB_DAT_T	*t_dat;		/* temporaly */
//	struct DB_DAT_T	*dat_ttt;

	dat_top = db->top;
	for (dat = dat_top; dat != 0; dat = dat_next) {
		if (dat->next == 0)	break;
		dat_next = dat->next;

		for (dat_i = db->end; dat_i != dat_top; dat_i = dat_i->prev) {
//		for (dat_i = dat->next; dat_i != db->end; dat_i = dat_i->next) {
			if (dat == dat_i)	break;
			if (dat_i == 0) {
				g_print("!!!!!!!!!  NULL  !!!!!!!!!!!!!\n");
				break;
			}
			c = comp(dat, dat_i);
#if 1
			if (c > 0) {
				//g_print("swap\n");
				if (dat_top == dat) {
					dat_top = dat_i;
				}
				if (db->top == dat)		db->top = dat_i;
				if (db->end == dat_i)	db->end = dat;

				if (dat->prev != 0)		dat->prev->next = dat_i;
				t_dat = dat->next;
				dat->next = dat_i->next;
				if (t_dat != dat_i)	dat_i->next = t_dat;
				else				dat_i->next = dat;
				if (dat_i->prev != dat)	dat_i->prev->next = dat;

				t_dat = dat_i->prev;
				dat_i->prev = dat->prev;
				if (dat->next != 0)		dat->next->prev = dat;
				dat->prev = t_dat;
				if (dat_i->next != 0)	dat_i->next->prev = dat_i;

//				dat_i = db->end;
				dat_next = dat_top;
				break;
			}
#endif
		}
		if (dat_i == dat) {
			dat_top = dat_top->next;
		}
	}

	return 1;
}
#endif
