/*  Note: You are free to use whatever license you want.
    Eventually you will be able to edit it within Glade. */

/*  gipmsg
 *  Copyright (C) <YEAR> <AUTHORS>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <gnome.h>
//#include <gtk/gtk.h>


#ifndef SIG_C__
extern GtkWidget	*winMain;
extern GtkWidget	*winSend;
extern GtkWidget	*winRecv;
extern GtkWidget	*winAbout;
extern GtkWidget	*winLog;
extern GtkWidget	*menu_select_group_sendUserList;
#endif /* SIG_C__ */


void
on_sendWin_destroy                     (GtkObject       *object,
                                        gpointer         user_data);

gboolean
on_sendWin_key_press_event      (GtkWidget       *widget,
								 GdkEventKey     *event,
								 gpointer         user_data);

gboolean
on_sendWinEv_button_press_event      (GtkWidget       *widget,
									GdkEventButton  *event,
									gpointer         user_data);

void
on_sendRefreshB_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_sendSendB_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvClose_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvSend_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_sendRefreshB_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_sendSendB_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvWinKaifu_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvWinKaifu_clicked_2                (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvWinKaifu_clicked_3                (GtkButton       *button,
										  gpointer         user_data);

void
on_sendUserList_click_column           (GtkCList        *clist,
                                        gint             column,
                                        gpointer         user_data);

gboolean
on_sendUserList_button_press_event      (GtkWidget       *widget,
										 GdkEventButton  *event,
										 gpointer         user_data);

gboolean
on_menu_select_group_cb(GtkObject *object, gpointer user_data);
//on_menu_select_group_cb(gpointer user_data);

void
on_buttonWin_destroy                   (GtkObject       *object,
                                        gpointer         user_data);

gboolean
on_buttonWin_delete_event              (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_buttonWin_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_buttonWin_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_buttonWinB_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonWin_size_request              (GtkWidget       *widget,
                                        GtkRequisition  *requisition,
                                        gpointer         user_data);

void
on_buttonWin_check_resize              (GtkContainer    *container,
                                        gpointer         user_data);

gboolean
on_buttonWin_button_press_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_buttonWin_size_allocate             (GtkWidget       *widget,
                                        GtkAllocation   *allocation,
                                        gpointer         user_data);

gboolean
on_buttonWinEv_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_buttonWinEv_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_logWin_destroy                      (GtkObject       *object,
                                        gpointer         user_data);

gboolean
on_logWin_delete_event                 (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_sendSendB_clicked_2                 (GtkButton       *button,
                                        gpointer         user_data);


void
on_sendSendB_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_recvWin_destroy                     (GtkObject       *object,
                                        gpointer         user_data);

#ifdef APPLET
gboolean
on_popupmenu_fuzai_applet_cb(AppletWidget *applet, gpointer user_data);
#endif /* APPLET */

gboolean
on_popupmenu_fuzai_cb(gpointer user_data);

void
on_mainmenu_setting_cb(gpointer user_data);

void
on_mainmenu_show_log_cb(gpointer user_data);

void
on_mainmenu_about_cb(gpointer user_data);

#if 0
void
on_mainmenu_absence_cb(gpointer user_data);
#endif

void
on_sendDohoTB_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void on_retryWin_retry_clicked(GtkButton       *button,
							   gpointer			user_data);

void on_retryWin_cancel_clicked(GtkButton       *button,
								gpointer		user_data);
