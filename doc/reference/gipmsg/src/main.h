#ifndef MAIN_H__
#define MAIN_H__


void main_quit(void);
#ifdef APPLET
extern gboolean is_applet;
#define applet_p() (is_applet != FALSE)	/* it's NOT lvalue */
#else /* APPLET */
#define applet_p() FALSE
#endif /* APPLET */


#endif /* MAIN_H__ */
