/*
 * kanji.h
 * $Id: kanji.h,v 1.1.1.2 1999/09/27 06:42:46 matty Exp $
 */
#ifndef KANJI_H /* [ */
#define KANJI_H

#ifndef is1sjis
#define is1sjis(c) (((0x81<=(unsigned char)(c))&&((unsigned char)(c)<=0x9f))||((0xe0<=(unsigned char)(c))&&((unsigned char)(c)<=0xfc)))
#endif
#ifndef is1euc
#define is1euc(c) ((0x81<=(unsigned char)(c))&&((unsigned char)(c)<=0xfe))
#endif

struct MB {
	enum {
		MB_INITIAL, /* initial state : ank chars */
		MB_KI1, /* detected 033 */
		MB_KI2, /* detected '$' */
		MB_INKANJI, /* detected '@' */
		MB_KO1, /* detected 033 */
		MB_KO2  /* detected '(' */
	} st;
	int hi; /* 1st byte of a multi byte character */
};

#define MB_CLEAR(mb) ((mb)->st = MB_INITIAL, (mb)->hi = 0)
#define KI_1 '$'
#define KO_1 '('

#define isskana(c) (((unsigned char)(c))>=0xa0&&((unsigned char)(c))<0xe0)

extern int kanji_in; /* default '@' */
extern int kanji_out; /* default 'J' */

extern int euc_to_sjis(struct MB *mb, int ch, int (*put)(int));
extern int jis_to_sjis(struct MB *mb, int ch, int (*put)(int));
extern int sjis_to_euc(struct MB *mb, int ch, int (*put)(int));
extern int sjis_to_jis(struct MB *mb, int ch, int (*put)(int));
extern char * stretos(char *dst, const char *src);
extern char * strstoe(char *dst, const char *src);
extern size_t strstoelen(const char *src);
extern char * strstoe2(char *dst, const char *src);

#endif /* ] KANJI_H */
