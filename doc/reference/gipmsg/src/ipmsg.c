#ifdef HAVE_CONFIG_H
#  include "../config.h"
#endif

#define IPMSG_C__

#include <gnome.h>
#ifdef APPLET
#  include <applet-widget.h>
#endif
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include "properties.h"
#include "win.h"
#include "sig.h"
#include "ipmsg.h"
#include "kanji.h"
#include "db.h"


#define	DEFAULT_IPMSG_PORT	(2425)
#define	IPMSG_MSG_TOKEN		"\a"

static gchar	buffer[MAX_SOCKBUF*2];
static gchar	t_buff[MAX_SOCKBUF];

#define HOSTNAME_LENGTH 256
 
static gchar	hostname[HOSTNAME_LENGTH];
static guint16 	ipmsg_port = DEFAULT_IPMSG_PORT;
static struct sockaddr_in	me;
static struct sockaddr_in	from;
static gint		fromlen;
static gint		len;
static gint		soc;			/* ソケット */
static fd_set	soc_mask;		/* mask */
static gint		soc_width;		/* width of the mask */


DB	*db;						/* db of user */
DB	*db_send;					/* db of sent message */
DB	*db_read;					/* db of read message */
DB	*db_broadcast;				/* db of broadcast addresses */
static gint		interrupt_tag;

static gint	nIPMsgFuzaiMode = 0;
gint	nIPMsgNonPopup = 0;
static gint	nIPMsgNonPopupTimeoutTag;
static gint	nIPMsgNonPopupFlag = 0;


#define	iskanji(c)	(((c)>=0x81 && (c)<=0x9F) || ((c)>=0xE0 && (c)<=0xFC))
#define	iskanji2(c)	((c)>=0x40 && (c)<=0xFC && (c)!=0x7F)

/*
 * stricmp (for ASCIIZ)
 */
int stricmp(char* s, char* d)
{
    while(*s){
		if (toupper(*s) < toupper(*d))		return -1;
		else if (toupper(*s) > toupper(*d))	return 1;
		s++;
		d++;
    }
    if (strlen(s) < strlen(d))	return 1;
    /* equal */
    return 0;
}


/*
 * strincmp (for ASCIIZ)
 */
int strincmp(char* s, char* d, int n)
{
    while(*s){
		if (n <= 0)	return 0;
		if (toupper(*s) < toupper(*d))		return -1;
		else if (toupper(*s) > toupper(*d))	return 1;
		n--;
		s++;
		d++;
    }
    if (strlen(s) < strlen(d))	return 1;
    /* equal */
    return 0;
}


void IPMsgFree_msg(struct IPMsgMessage_T *msg)
{
	g_free(msg->serial);
	g_free(msg->user);
	g_free(msg->machine);
	g_free(msg->group);
	g_free(msg->appendix);
	g_free(msg);
}
void IPMsgFree_list(struct IPMsgList_T *list)
{
	g_free(list->nick);
	g_free(list->machine);
	g_free(list->user);
	g_free(list->group);
	g_free(list);
}
int IPMsgDBEachFree_list(struct DB_DAT_T *dat, void *para)
{
	IPMsgFree_list(dat->data);
	return 0;
}
void IPMsgFree_smsg(struct IPMsgSentMsg_T *smsg)
{
	g_free(smsg->user);
	g_free(smsg->machine);
	g_free(smsg->msg);
	g_free(smsg);
}
void IPMsgFree_rmsg(struct IPMsgReadMsgData_T *rmsg)
{
	g_free(rmsg->msg);
	g_free(rmsg);
}


void IPMsgSetup(guint16 port)
{
	// get hostname
	if (gethostname(hostname, HOSTNAME_LENGTH) == -1) {
		g_error(_("cannot find hostname\n"));
	}
    if (strchr(hostname, '.') != NULL)	*strchr(hostname, '.') = '\0';
	// alloc database
	db = db_new(NULL);
	db_send = db_new(NULL);
	db_read = db_new(NULL);
	db_broadcast = db_new(NULL);
	// set IP Messenger port
	if (port != 0)	ipmsg_port = port;
}

void IPMsgQuit(void)
{
	IPMsgSendBroadcast(IPMSG_BR_EXIT, IPMsgGetNickWithAbsence());
}


static void IPMsgBcastAppend(char *p)
{
	struct in_addr *baddress;

	baddress = g_malloc(sizeof(baddress));
	if (inet_aton(p, baddress) == 0) {
		if (*p != '\0')
			g_warning(_("inet_aton: broadcast address[%s] cannot convert to binary.\n"), p);
	}
	else {
		dbd_append(db_broadcast, baddress);
	}
}


void IPMsgSetBcasts(char *lbcast, char *bcasts)
{
	gchar	*p;
	gchar	**parray, **q;

	db_delete(db_broadcast);
	
	/* appned broadcast address of localnet */
	IPMsgBcastAppend(lbcast);

	/* append broadcast address of others */
	q = parray = g_strsplit(bcasts, ",", 256);
	for (p = *q; p; p = *(++q)) {
		IPMsgBcastAppend(p);
	}
	g_strfreev(parray);
}


// broadcast for dial-up users
int IPMsgDBEachSendDialup(struct DB_DAT_T *data, void *para) 
{
	struct {
		guint32 com ;
		gchar *data ;
	} *sendbroadcast_para = para;

	if(((IPMsgList *)data)->dialup) 
		IPMsgSend(&((IPMsgList *)data)->from, sendbroadcast_para->com, sendbroadcast_para->data);
	return 0;
}


int IPMsgDBEachSendBroadcast(struct DB_DAT_T *baddress, void *para) 
{
	struct sockaddr_in	bcast;
	struct {
		guint32 com ;
		gchar *data ;
	} *sendbroadcast_para = para;
  
	memset((gchar*)&bcast, 0, sizeof(bcast));
	bcast.sin_family = AF_INET;
	bcast.sin_port = htons(ipmsg_port);
	bcast.sin_addr = *(struct in_addr *)(baddress->data);
	IPMsgSend(&bcast, sendbroadcast_para->com, sendbroadcast_para->data);
	return 0;
}

void IPMsgSendBroadcast(guint32 com, gchar *data)
{
	struct {
		guint32 com;
		gchar *data;
	} sendbroadcast_para;
 
	sendbroadcast_para.com = com;
	sendbroadcast_para.data = data;
	db_foreach(db_broadcast, IPMsgDBEachSendBroadcast, &sendbroadcast_para);
	db_foreach(db, IPMsgDBEachSendDialup, &sendbroadcast_para);
}

gint IPMsgSendSetSerial(struct sockaddr_in *dest, gint serial, guint32 com, gchar *data)
{
	gint		len, i;
	gchar		*p;

	memset(buffer, 0, MAX_SOCKBUF);
	sprintf(buffer, "1:%u:%s:%s:%u:%s", serial, option.user, hostname, com, data);
	len = strlen(buffer)+1;
	for (i = 0, p = buffer; i < len-1; i++, p++) {
		if (*p == '\a')	*p = '\0';
	}
	sendto(soc, buffer, len, 0, dest, sizeof(struct sockaddr_in));
	//g_print("SEND:%s\n", buffer);
	return serial;
}
gint IPMsgSend(struct sockaddr_in *dest, guint32 com, gchar *data)
{
	time_t now = time(NULL);	/* packet no */
	static guint	last_time = 0;

	memset(buffer, 0, MAX_SOCKBUF);
	if ((guint)now <= last_time)	now = ++last_time;
	last_time = now;
	return IPMsgSendSetSerial(dest, now, com, data);
}

void IPMsgLogin(void)
{
	int optval = 1;

	if (option.localnet_bcast == NULL)	option.localnet_bcast = "255.255.255.255";
	IPMsgSetBcasts(option.localnet_bcast, option.broadcasts);
	if ((soc = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		g_error(_("socket alloc failed.\n"));
	}
	memset((gchar*)&me, 0, sizeof(me));
	me.sin_addr.s_addr = INADDR_ANY;
	me.sin_family = AF_INET;
	me.sin_port = htons(ipmsg_port);
	setsockopt(soc, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));	/* ブロードキャストを有効にする */
	if (bind(soc, &me, sizeof(me)) == -1) {
		g_error(_("could not bind.\n"));
	}
	IPMsgSendBroadcast(IPMSG_BR_ENTRY, IPMsgGetNickWithAbsence());

	interrupt_tag = gdk_input_add(soc, GDK_INPUT_READ, IPMsgMain, NULL);

	soc_width = soc+1;
	FD_ZERO(&soc_mask);
	FD_SET(soc, &soc_mask);
}


static const char *date_week[7] = {
	N_("Sun"), N_("Mon"), N_("Tue"), N_("Wed"), N_("Thu"), N_("Fri"), N_("Sat"),
};
static const char *date_month[12] = {
	N_("Jan"), N_("Feb"), N_("Mar"), N_("Apr"), N_("May"), N_("Jun"), N_("Jul"), N_("Aug"), N_("Sep"), N_("Oct"), N_("Nov"), N_("Dec"),
};

void IPMsgGotMsg(struct IPMsgMessage_T *msg)
{
	GtkWidget	*recvWin;
	GtkWidget	*recvText;
	time_t now = time(NULL);
	struct tm lc = *localtime(&now);
#define	INSERT_BUFF_SIZE	512
#if 0
	gchar		insert_buff[INSERT_BUFF_SIZE+1];
#endif
	gint		text_len;
	gchar		*pStr, *pp;
	IPMsgReadMsgData	*readmsg;
	FILE		*fp;
	gint		i;
	GtkWidget *urlWidget;			/* for TEST */
	DB_DAT		*dbd;

	if ((dbd = (DB_DAT*)db_foreach(db, IPMsgSearchUser, msg)) != 0)
		((IPMsgList*)(dbd->data))->recv++;

	//sprintf(t_buff, "%s[%s] (%s/%s)\nat %s %s %2u %2u:%02u:%02u %4u", msg->user, "", msg->machine, inet_ntoa(from.sin_addr)
	sprintf(t_buff, "%s[%s]\nfrom %s/%s\nat %s %s %2u %2u:%02u:%02u %4u", msg->user, "", msg->machine, inet_ntoa(from.sin_addr)
			, date_week[lc.tm_wday], date_month[lc.tm_mon], lc.tm_mday
			, lc.tm_hour, lc.tm_min, lc.tm_sec, lc.tm_year+1900);

	if (option.log && (msg->com2&IPMSG_NOLOGOPT) == 0) {
		if ((fp = fopen(option.log_fname, "a+")) != NULL) {
			fputs("================================\nFrom: ", fp);
			fputs(t_buff, fp);
			if ((msg->com2&IPMSG_SECRETOPT))	fputs(" (seal)", fp);
			fputs("\n--------------------------------\n", fp);
			fputs(msg->appendix, fp);
			fputs("\n\n", fp);
			fclose(fp);
		}
	}

	recvWin = create_recvWin();
	if ((msg->com2&IPMSG_BROADCASTOPT)) {
		gtk_frame_set_label(GTK_FRAME(get_widget(GTK_WIDGET(recvWin), "frame6")), _("Broadcast From..."));
	}
	else if ((msg->com2&IPMSG_MULTICASTOPT)) {
		gtk_frame_set_label(GTK_FRAME(get_widget(GTK_WIDGET(recvWin), "frame6")), _("Multicast From..."));
	}
	gtk_label_set(GTK_LABEL(get_widget(GTK_WIDGET(recvWin), "recvMsg")), t_buff);
	recvText = get_widget(GTK_WIDGET(recvWin), "recvText");

	gtk_text_freeze(GTK_TEXT(recvText));
	text_len = strlen(msg->appendix);
	gtk_text_insert(GTK_TEXT(recvText), NULL, NULL, NULL,
					msg->appendix, -1);
	gtk_text_thaw (GTK_TEXT (recvText));

	/* db登録 */
	readmsg = (IPMsgReadMsgData*)g_malloc(sizeof(struct IPMsgReadMsgData_T));
	readmsg->from = msg->from;
	readmsg->serial = atoi(msg->serial);
	readmsg->msg = g_strdup(msg->appendix);
	readmsg->win = recvWin;
	readmsg->db_tag = dbd_append(db_read, readmsg);
	readmsg->have_url = 0;
	readmsg->shown = 0;

	for (i = 1, pStr = msg->appendix; *pStr; pStr++) {
		if (strincmp(pStr, "http://", 7) == 0 || strincmp(pStr, "ftp://", 6) == 0) {
			pp = t_buff;
			while (1) {
				//if (*pStr == '\0' || (*pStr != ':' && *pStr != '/' && !isalnum(*pStr))) {
				if (*pStr == '\0' || !isgraph(*pStr)) {
					*pp = '\0';
					urlWidget = gnome_href_new (t_buff, t_buff);
					gtk_tooltips_set_tip (gtk_tooltips_new(), urlWidget, t_buff, NULL);
					gtk_widget_show (urlWidget);
					gtk_box_pack_start (GTK_BOX (get_widget(GTK_WIDGET(recvWin), "recvWinURLBox")), urlWidget, FALSE, FALSE, 0);
					readmsg->have_url = 1;
					break;
				}
				*pp++ = *pStr++;
			}
		}
	}

	if ((msg->com2&IPMSG_SENDCHECKOPT)) {
		IPMsgSend(&msg->from, IPMSG_RECVMSG, msg->serial);
	}
	/* if I am in absence mode, I'll send absence message */
	if (nIPMsgFuzaiMode > 0 && !(msg->com2&IPMSG_AUTORETOPT)) {
		pStr = g_strdup(option.absence_text[nIPMsgFuzaiMode-1]);
		IPMsgSend(&msg->from, (IPMSG_SENDMSG|IPMSG_AUTORETOPT), stretos(pStr, pStr));
		g_free(pStr);
	}

	if ((msg->com2&IPMSG_SECRETOPT)) {
		gtk_widget_show(get_widget(GTK_WIDGET(recvWin), "recvWinKaifu"));
		gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "vpaned1"));
		gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvWinScrollbar"));
		gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvText"));
		//gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvCloseB"));
		//gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvSendB"));
		//gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvWinCite"));
		gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvWinURLScrollbar"));
		gtk_widget_hide(get_widget(GTK_WIDGET(recvWin), "recvWinButtonBox"));
		gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(recvWin), "recvCloseB")), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(get_widget(GTK_WIDGET(recvWin), "recvSendB")), FALSE);
		gtk_signal_connect (GTK_OBJECT(get_widget(GTK_WIDGET(recvWin), "recvWinKaifu")), "clicked",
							GTK_SIGNAL_FUNC (on_recvWinKaifu_clicked_2),
							readmsg);
		if (readmsg->have_url) {
			gtk_signal_connect (GTK_OBJECT(get_widget(GTK_WIDGET(recvWin), "recvWinKaifu")), "clicked",
								GTK_SIGNAL_FUNC (on_recvWinKaifu_clicked_3),
								recvWin);
		}
	}
	if (option.non_popup) {
		if (nIPMsgNonPopup == 0) {
			nIPMsgNonPopupTimeoutTag = gtk_timeout_add(1000, IPMsgNonPopupTimeout, NULL);
			nIPMsgNonPopupFlag = 0;
		}
		nIPMsgNonPopup++;
	}
	else {
		readmsg->shown = 1;
		gtk_widget_show (recvWin);
		if ((msg->com2&IPMSG_SECRETOPT)) {
			gtk_widget_grab_focus(get_widget(GTK_WIDGET(recvWin), "recvWinKaifu")); /* focusを移す */
		}
		else {
			gtk_widget_grab_focus(get_widget(GTK_WIDGET(recvWin), "recvText")); /* focusを移す */
			if (readmsg->have_url)
				gtk_widget_show(get_widget(GTK_WIDGET(recvWin), "recvWinURLScrollbar"));
		}
	}
	if (option.beep)	gdk_beep();
	if (option.snd)		gnome_sound_play(option.snd_fname);
}


/*
 * 送った封書が開封された
 */
void IPMsgReadMsg(struct IPMsgMessage_T *msg)
{
	GtkWidget	*kaifuWin;
	time_t now = time(NULL);
	struct tm lc = *localtime(&now);

	if (option.check_seal) {
		kaifuWin = create_kaifuWin();
		sprintf(t_buff, _("%s[%s] (%s) - gipmsg"), msg->user, "", msg->machine);
		gtk_window_set_title(GTK_WINDOW (kaifuWin), t_buff);
		sprintf(t_buff, _("%s[%s] (%s)\nopened\n  (at %s %2u %2u:%02u)  ")
				, msg->user, "", msg->machine
				, date_month[lc.tm_mon], lc.tm_mday
				, lc.tm_hour, lc.tm_min);
		gtk_label_set(GTK_LABEL(get_widget(GTK_WIDGET(kaifuWin), "kaifuWinMsg")), t_buff);
		gtk_widget_show(kaifuWin);
		gtk_widget_grab_focus(get_widget(GTK_WIDGET(kaifuWin), "kaifuWinOK")); /* focusを移す */
	}
}


void IPMsgUserEntry(struct IPMsgMessage_T *msg)
{
	IPMsgList	*data;
	DB_DAT		*dbd;

	if (db_foreach(db, IPMsgSearchUser, msg) != 0)	return;
	data = g_malloc(sizeof(IPMsgList));
	data->user = g_strdup(msg->user);
	data->machine = g_strdup(msg->machine);
	data->nick = g_strdup(msg->appendix);
	data->group = g_strdup(msg->group);
	data->from = msg->from;
	data->send = 0;
	data->recv = 0;
	data->dialup = ((msg->com2) & IPMSG_DIALUPOPT) ? TRUE : FALSE;
	dbd = dbd_append(db, data);
	if (winSend) {
		IPMsgSendWinDraw(dbd, winSend);
		sprintf(t_buff, "%s: %d", _("User num"), db->num);
		gtk_label_set(GTK_LABEL(get_widget(GTK_WIDGET(winSend), "sendUserNum")), t_buff);
	}
}


void IPMsgUserExit(struct IPMsgMessage_T *msg)
{
	DB_DAT		*dbd;
	GtkCList*		clist;

	if ((dbd = (DB_DAT*)db_foreach(db, IPMsgSearchUser, msg)) != 0) {
		if (winSend) {
			clist = GTK_CLIST(get_widget(GTK_WIDGET(winSend), "sendUserList"));
			gtk_clist_remove(clist, gtk_clist_find_row_from_data(clist, dbd->data));
			sprintf(t_buff, "%s: %d", _("User num"), db->num);
			gtk_label_set(GTK_LABEL(get_widget(GTK_WIDGET(winSend), "sendUserNum")), t_buff);
		}
		IPMsgFree_list(dbd->data);
		dbd_free(db, dbd);
	}
}

void IPMsgSwitch(struct IPMsgMessage_T *msg)
{
	IPMsgList	*data;
	DB_DAT		*dbd;
	IPMsgSentMsg	*smsg;
	GtkCList*		clist;

	switch (msg->com) {
	case IPMSG_NOOPERATION:		/* 無操作 */
		//g_print("[ IPMSG_BR_NOOPERATION ]\n");
		break;
	case IPMSG_BR_ENTRY:		/* サービスにエントリ（起動時にBroadcast） */
		//g_print("[ IPMSG_BR_ENTRY ]\n");
		IPMsgUserEntry(msg);
		if (nIPMsgFuzaiMode > 0)	IPMsgSend(&from, (IPMSG_ANSENTRY|IPMSG_ABSENCEOPT), IPMsgGetNickWithAbsence());
		else						IPMsgSend(&from, IPMSG_ANSENTRY, IPMsgGetNickWithAbsence());
		break;
	case IPMSG_BR_EXIT:			/* サービスから抜ける（終了時にBroadcast） */
		//g_print("[ IPMSG_BR_EXIT ]\n");
		IPMsgUserExit(msg);
		break;
	case IPMSG_ANSENTRY:		/* エントリを認識したことを通知 */
		//g_print("[ IPMSG_ANSENTRY ]\n");
		IPMsgUserEntry(msg);
		break;
	case IPMSG_BR_ABSENCE:		/* 不在モード変更 */
		//g_print("[ IPMSG_BR_ABSENCE ]\n");
		if ((dbd = (DB_DAT*)db_foreach(db, IPMsgSearchUser, msg)) != 0) {
			if (winSend) {
				clist = GTK_CLIST(get_widget(GTK_WIDGET(winSend), "sendUserList"));
				gtk_clist_set_text(clist, gtk_clist_find_row_from_data(clist, dbd->data), 0, msg->appendix);
			}
			data = dbd->data;
			g_free(data->nick);
			data->nick = g_strdup(msg->appendix);
		}
		break;
	case IPMSG_SENDMSG:			/* メッセージの送信 */
		//g_print("[ IPMSG_SENDMSG ]\n");
		IPMsgGotMsg(msg);
		break;
	case IPMSG_RECVMSG:			/* メッセージの受信確認 */
		//g_print("[ IPMSG_RECVMSG ]\n");
		if ((smsg = (IPMsgSentMsg*)db_foreach(db_send, IPMsgSearchSerial, msg->appendix)) != 0) { /* 以前送ったパケットへの返事か? */
			//g_print("delete message\n");
			gtk_timeout_remove(smsg->timeout_tag);
			dbd_free(db_send, smsg->db_tag);
			IPMsgFree_smsg(smsg);
		}
		else {
			g_warning(_("IPMSG_RECVMSG: unknown message"));
		}
		break;
	case IPMSG_READMSG:			/* 封書の開封通知 */
		//g_print("[ IPMSG_READMSG ]\n");
		IPMsgReadMsg(msg);
		break;
	default:
	}
	//if (!(msg->com&IPMSG_SENDMSG))	IPMsgFree_msg(msg);
	IPMsgFree_msg(msg);
}


void IPMsgMain(gpointer data, gint source, GdkInputCondition condition)
{
	fd_set	readOk;
	struct timeval tv;
	IPMsgMessage	*msg;
	gchar	*pchr;
	gint	nSep;

	gdk_input_remove(interrupt_tag);

	tv.tv_sec = tv.tv_usec = 0;
	while (1) {
		readOk = soc_mask;
		if (!select(soc_width, (fd_set*)&readOk, NULL, NULL, &tv))	break;
		if (FD_ISSET(soc, &readOk)) { /* ソケットにデータがあるか */
			fromlen = sizeof(from);
			if ((len = recvfrom(soc, t_buff, MAX_SOCKBUF, 0, &from, &fromlen)) == -1)
				continue;
			pchr = t_buff;
			for (nSep = 0; len > 1; len--, pchr++) {
				if (nSep < 5) {
					if (*pchr == ':') {
						*pchr = '\a';
						nSep++;
					}
				}
				else {
					if (*pchr == '\0')	*pchr = '\a';
				}
			}
			strstoe(buffer, t_buff);
			if (strcmp(strtok(buffer, IPMSG_MSG_TOKEN), "1") != 0) { /* version確認 */
				g_warning(_("illegal version of ipmsg protocol"));
			}
			/* tokenを分ける */
			msg = g_malloc(sizeof(IPMsgMessage));
			msg->from = from;
			msg->from.sin_family = AF_INET;
			msg->from.sin_port = htons(ipmsg_port);
			msg->fromlen = fromlen;
			msg->serial = g_strdup(strtok(NULL, IPMSG_MSG_TOKEN));
			msg->user = g_strdup(strtok(NULL, IPMSG_MSG_TOKEN));
			msg->machine = g_strdup(strtok(NULL, IPMSG_MSG_TOKEN));
			msg->com = atoi(strtok(NULL, IPMSG_MSG_TOKEN));
			msg->com2 = msg->com&0xffffff00;
			msg->com &= 0xff;
			msg->appendix = g_strdup(strtok(NULL, IPMSG_MSG_TOKEN));
			if ((pchr = strtok(NULL, IPMSG_MSG_TOKEN)) != 0)	msg->group = g_strdup(pchr);
			else												msg->group = g_strdup("");
			IPMsgSwitch(msg);	/* 各処理へ */
			IPMsgResetTooltip();
		}
		else
			break;
	}
	//gtk_clist_thaw(GTK_CLIST(sendUserList));
	interrupt_tag = gdk_input_add(soc, GDK_INPUT_READ, IPMsgMain, NULL);
}

int IPMsgSearchUser(struct DB_DAT_T *dat, void *para)
{
	IPMsgMessage	*msg;
	IPMsgList		*data;
	
	msg = para;
	data = dat->data;

	if (memcmp(&data->from.sin_addr, &msg->from.sin_addr, 4) != 0)	return 0;
	if (strcmp(data->user, msg->user) != 0)	return 0;
	if (strcmp(data->machine, msg->machine) != 0)	return 0;

	return (int)dat;
}
int IPMsgSearchSerial(struct DB_DAT_T *dat, void *para)
{
	guint	serial;
	IPMsgSentMsg	*data;
	
	serial = atoi(para);
	data = dat->data;
	if (serial == data->serial)	return (int)data;
	return 0;
}


int IPMsgSearchReadMsg(struct DB_DAT_T *dat, void *para)
{
	IPMsgReadMsgData	*data;
	
	data = dat->data;
	if (para == data->win)	return (int)data;
	return 0;
}
int IPMsgSearchReadFrom(struct DB_DAT_T *dat, void *para)
{
	IPMsgReadMsgData	*rmsg;
	IPMsgList		*data;
	
	rmsg = para;
	data = dat->data;
	if (memcmp(&data->from.sin_addr, &rmsg->from.sin_addr, 4) != 0)	return 0;
	return (int)data;
}


int IPMsgSearchNick(struct DB_DAT_T *dat, void *para)
{
	IPMsgList		*data;

	data = dat->data;
	if (strncmp(data->nick, (gchar*)para, strlen((gchar*)para)) == 0)	return (int)data;
	return 0;
}



int IPMsgSendWinDraw(struct DB_DAT_T *dat, void *para)
{
	GtkWidget	*sendUserList;
	GtkWidget	*sendWin;
	gchar	*item[5];
	IPMsgList		*data;

	data = dat->data;
	sendWin = para;
	item[0] = data->nick;
	item[1] = data->user;
	item[2] = data->machine;
	item[3] = data->group;
	item[4] = inet_ntoa(data->from.sin_addr);
	sendUserList = get_widget(GTK_WIDGET(sendWin), "sendUserList");
	gtk_clist_set_row_data(GTK_CLIST(sendUserList), gtk_clist_append(GTK_CLIST(sendUserList), item), (gpointer)data);

	return 0;
}


gint IPMsgReSendData(gpointer dat)
{
	IPMsgSentMsg	*smsg = dat;
	GtkWidget	*dialog;

	smsg->retry++;
	if (smsg->retry >= IPMSG_RETRY_TIMES) {
		gtk_timeout_remove(smsg->timeout_tag);
		dialog = create_retry_win(smsg);
		gtk_widget_show_all(dialog);
		return TRUE;
	}
	IPMsgSendSetSerial(&smsg->to, smsg->serial, smsg->com|IPMSG_RETRYOPT, smsg->msg);
	return TRUE;
}

int IPMsgSendData(IPMsgList	*data, void *sendWin, guint32 flag)
{
	time_t now = time(NULL);
	struct tm lc = *localtime(&now);
	FILE	*fp;
	GtkToggleButton	*t_button;
	gchar		*str;
	IPMsgSentMsg	*smsg;
	GtkWidget*	textbox;

	//g_print("user:%s machine:%s\n", data->user, data->machine);
	data->send++;
	textbox = get_widget(GTK_WIDGET(sendWin), "sendTextBox");
	str = gtk_editable_get_chars(GTK_EDITABLE(textbox), 0, -1);
	if (option.log && (flag&IPMSG_NOLOGOPT) == 0) {
		if ((fp = fopen(option.log_fname, "a+")) != NULL) {
			sprintf(t_buff, "%s[%s] (%s/%s)\nat %s %s %2u %2u:%02u:%02u %4u", data->user, "", data->machine, inet_ntoa(data->from.sin_addr)
					, date_week[lc.tm_wday], date_month[lc.tm_mon], lc.tm_mday
					, lc.tm_hour, lc.tm_min, lc.tm_sec, lc.tm_year+1900);

			fputs("================================\nTo: ", fp);
			fputs(t_buff, fp);
			if ((flag&IPMSG_SECRETOPT))	fputs(" (seal)", fp);
			fputs("\n--------------------------------\n", fp);
			fputs(str, fp);
			fputs("\n\n", fp);
			fclose(fp);
		}
	}
	stretos(str, str);
	smsg = g_malloc(sizeof(IPMsgSentMsg));
	smsg->user = g_strdup(data->user);
	smsg->machine = g_strdup(data->machine);
	smsg->to = data->from;
	smsg->com = (IPMSG_SENDMSG|IPMSG_SENDCHECKOPT)|flag;
	t_button = GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(sendWin), "sendFushoTB"));
	if (t_button->active != 0)	smsg->com |= IPMSG_SECRETOPT;
	smsg->msg = str;
	smsg->serial = IPMsgSend(&smsg->to, smsg->com, str);
	smsg->timeout_tag = gtk_timeout_add(IPMSG_TIMERINTERVAL, IPMsgReSendData, smsg);
	smsg->retry = 0;
	smsg->db_tag = dbd_append(db_send, smsg);
	//g_free(str); 配送確認が届いた後にfreeする

	return 0;
}
int IPMsgSendDataBroadcast(void *sendWin, guint32 flag)
{
	gchar		*str;

	str = gtk_editable_get_chars(GTK_EDITABLE(get_widget(GTK_WIDGET(sendWin), "sendTextBox")), 0, -1);
	stretos(str, str);
	if (GTK_TOGGLE_BUTTON(get_widget(GTK_WIDGET(sendWin), "sendFushoTB"))->active != 0)	flag |= IPMSG_SECRETOPT;
	IPMsgSendBroadcast((IPMSG_SENDMSG|flag), str);
	return 0;
}

#if 0
int IPMsgUserListSortName(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i)
{
	IPMsgList	*data[2];

	data[0] = dat->data;
	data[1] = dat_i->data;
	return strcmp(data[0]->nick, data[1]->nick);
}

int IPMsgUserListSortUser(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i)
{
	IPMsgList	*data[2];
	data[0] = dat->data;
	data[1] = dat_i->data;
	return strcmp(data[0]->user, data[1]->user);
}

int IPMsgUserListSortMachine(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i)
{
	IPMsgList	*data[2];
	data[0] = dat->data;
	data[1] = dat_i->data;
	return strcmp(data[0]->machine, data[1]->machine);
}

int IPMsgUserListSortGroup(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i)
{
	IPMsgList	*data[2];
	data[0] = dat->data;
	data[1] = dat_i->data;
	return strcmp(data[0]->group, data[1]->group);
}

int IPMsgUserListSortIP(struct DB_DAT_T *dat, struct DB_DAT_T *dat_i)
{
	IPMsgList	*data[2];
	data[0] = dat->data;
	data[1] = dat_i->data;
	return memcmp(&(data[0]->from.sin_addr), &(data[1]->from.sin_addr), 4);
}
#endif


void IPMsgResetTooltip(void)
{
	gchar	buf[64];
	if (option.non_popup)
		sprintf(buf, _("GIPMSG - port %u\n %d users logged on\n You've %d messages"), ipmsg_port, db->num, nIPMsgNonPopup);
	else
		sprintf(buf, _("GIPMSG - port %u\n %d users logged on"), ipmsg_port, db->num);
	gtk_tooltips_disable (GTK_TOOLTIPS(get_widget(GTK_WIDGET(winMain), "tooltips")));
	gtk_tooltips_set_tip (GTK_TOOLTIPS(get_widget(GTK_WIDGET(winMain), "tooltips")), winMain, buf, buf);
	gtk_tooltips_enable (GTK_TOOLTIPS(get_widget(GTK_WIDGET(winMain), "tooltips")));
}


GtkWidget* IPMsgOpenSendWin(void)
{
	GtkWidget	*sendWin;
	gchar	buf[32];

	sendWin = create_sendWin ();
	sprintf(buf, "%s: %d", _("User num"), db->num);
	gtk_label_set(GTK_LABEL(get_widget(GTK_WIDGET(sendWin), "sendUserNum")), buf);
	gtk_widget_show (sendWin);
	gtk_clist_freeze(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")));
	db_foreach(db, IPMsgSendWinDraw, sendWin);
	gtk_clist_thaw(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")));
	gtk_clist_set_sort_column(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")), option.default_sort_column);
	gtk_clist_set_sort_type(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")), option.default_sort_type);
	gtk_clist_set_compare_func(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")), sendWin_clist_comp);
	gtk_clist_sort(GTK_CLIST(get_widget(GTK_WIDGET(sendWin), "sendUserList")));
	return sendWin;
}


gchar *IPMsgGetNickWithAbsence(void)
{
	static gchar	buf[64];

	if (nIPMsgFuzaiMode > 0)	sprintf(buf, "%s[%s]%c%s", option.nick, option.absence[nIPMsgFuzaiMode-1], '\a', option.group);
	else						sprintf(buf, "%s%c%s", option.nick, '\a', option.group);
	stretos(buf, buf);
	return (char*)buf;
}


void IPMsgFuzaiChange(gint mode)
{
	nIPMsgFuzaiMode = mode;
	if (mode == 0) {
		IPMsgSendBroadcast(IPMSG_BR_ABSENCE, IPMsgGetNickWithAbsence());
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
	else {
		IPMsgSendBroadcast((IPMSG_BR_ABSENCE|IPMSG_ABSENCEOPT), IPMsgGetNickWithAbsence());
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
}


GList *IPMsgGetGroupList(void)
{
	IPMsgList	*list;
	DB_DAT		*dbd;
	GList		*glist = NULL;

	for (dbd = db->top; dbd != 0; dbd = dbd->next) {
		list = dbd->data;
		if (g_list_find_custom(glist, list->group, (GCompareFunc)strcmp) == NULL) {
			//glist = g_list_append(glist, ipmsg_list->group);
			glist = g_list_insert_sorted(glist, list->group, (GCompareFunc)strcmp);
		}
	}
	//g_list_sort(glist, (GCompareFunc)strcmp);   g_list_sort() has bug!?
	return glist;
}


gint IPMsgNonPopupTimeout(gpointer dat)
{
	if (nIPMsgNonPopupFlag) {
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
	else {
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
	nIPMsgNonPopupFlag = !nIPMsgNonPopupFlag;
	return TRUE;
}


void IPMsgOpenNonPopup(void)
{
	DB_DAT		*dbd;
	IPMsgReadMsgData	*rmsg;

	gtk_timeout_remove(nIPMsgNonPopupTimeoutTag);
	if (nIPMsgFuzaiMode == 0) {
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
	else {
		gtk_widget_hide(get_widget(GTK_WIDGET(winMain), "buttonWinPix"));
		gtk_widget_show(get_widget(GTK_WIDGET(winMain), "buttonWinPix2"));
	}
	for (dbd = db_read->top; dbd != 0; dbd = dbd->next) {
		rmsg = dbd->data;
		rmsg->shown = 1;
		gtk_widget_show (rmsg->win);
	}
	nIPMsgNonPopup = 0;
	IPMsgResetTooltip();
}


gint sendWin_clist_comp(GtkCList *clist,
						gconstpointer ptr1,
						gconstpointer ptr2)
{
	char *text1 = NULL;
	char *text2 = NULL;
	GtkCListRow *row1 = (GtkCListRow *) ptr1;
	GtkCListRow *row2 = (GtkCListRow *) ptr2;

	gulong	addr1;
	gulong	addr2;
	gint	fail;
	gint	true;

	switch (row1->cell[clist->sort_column].type)
    {
    case GTK_CELL_TEXT:
		text1 = GTK_CELL_TEXT (row1->cell[clist->sort_column])->text;
		break;
    case GTK_CELL_PIXTEXT:
		text1 = GTK_CELL_PIXTEXT (row1->cell[clist->sort_column])->text;
		break;
    default:
		break;
    }
 
	switch (row2->cell[clist->sort_column].type)
    {
    case GTK_CELL_TEXT:
		text2 = GTK_CELL_TEXT (row2->cell[clist->sort_column])->text;
		break;
    case GTK_CELL_PIXTEXT:
		text2 = GTK_CELL_PIXTEXT (row2->cell[clist->sort_column])->text;
		break;
    default:
		break;
    }

	if (!text2)
		return (text1 != NULL);

	if (!text1)
		return -1;

	if (clist->sort_type == GTK_SORT_ASCENDING) {
		fail = -1;
		true = 1;
	} else {
		fail = 1;
		true = -1;
	}

	switch (clist->sort_column) {
	case 4:						/* IP */
		addr1 = inet_addr (text1);
		addr2 = inet_addr (text2);
		if (addr1 == addr2)	return 0;
		if (addr1 < addr2)	return -1;
		return 1;
	}
	return strcmp (text1, text2);
}


